import {Component, Input} from '@angular/core';
import {Tournament} from "../../../shared/models/tournament";
import {Match} from "../../../shared/models/match";

@Component({
  selector: 'tournament-matches',
  templateUrl: './tournament-matches.component.html',
  styleUrls: ['./tournament-matches.component.less']
})
export class TournamentMatchesComponent {
  @Input()
  tournament: Tournament = new Tournament({tournamentName: ''});

  get tournamentMatches(): Match[] {
    return this.tournament.matches;
  }
}
