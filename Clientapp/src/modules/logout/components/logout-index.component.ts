import { Component } from '@angular/core';
import {AuthenticationService} from "../../shared/services/authentication.service";

@Component({
  selector: 'app-logout-index',
  templateUrl: './logout-index.component.html',
  styleUrls: ['./logout-index.component.less']
})
export class LogoutIndexComponent {

  constructor(private authenticationService: AuthenticationService) {
  }

  async ngOnInit(): Promise<void> {
    await this.authenticationService.logout();
  }

}
