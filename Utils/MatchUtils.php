<?php

namespace Pickomenka\Utils;

use Pickomenka\Models\MatchModel;
use Pickomenka\Models\TeamModel;

class MatchUtils
{
    /**
     * @param MatchModel $match
     * @param MatchModel[] $matchReferential
     * @param TeamModel[] $teamReferential
     * @return TeamModel|null
     */
    public static function getMatchTeam1(MatchModel $match, array $matchReferential, array $teamReferential): TeamModel | null {
        if (!$match->isMatchDeterminedByParents()) {
            if ($match->getTeamId1() === null)
                return null;

            foreach ($teamReferential as $team) {
                if ($team->getTeamId() === $match->getTeamId1())
                    return $team;
            }

            return null;
        }

        $parentMatch = null;
        foreach ($matchReferential as $matchRef) {
            if ($matchRef->getMatchId() === $match->getParentMatchId1()) {
                $parentMatch = $matchRef;
                break;
            }
        }

        if ($parentMatch === null)
            return null;

        return self::getMatchWinner($parentMatch, $matchReferential, $teamReferential);
    }

    /**
     * @param MatchModel $match
     * @param MatchModel[] $matchReferential
     * @param TeamModel[] $teamReferential
     * @return TeamModel|null
     */
    public static function getMatchTeam2(MatchModel $match, array $matchReferential, array $teamReferential): TeamModel | null {
        if (!$match->isMatchDeterminedByParents()) {
            if ($match->getTeamId2() === null)
                return null;

            foreach ($teamReferential as $team) {
                if ($team->getTeamId() === $match->getTeamId2())
                    return $team;
            }

            return null;
        }

        $parentMatch = null;
        foreach ($matchReferential as $matchRef) {
            if ($matchRef->getMatchId() === $match->getParentMatchId2()) {
                $parentMatch = $matchRef;
                break;
            }
        }

        if ($parentMatch === null)
            return null;

        return self::getMatchWinner($parentMatch, $matchReferential, $teamReferential);
    }

    /**
     * @param MatchModel $match
     * @param MatchModel[] $matchReferential
     * @param TeamModel[] $teamReferential
     * @return TeamModel|null
     */
    public static function getMatchWinner(MatchModel $match, array $matchReferential, array $teamReferential): TeamModel | null {
        $team1 = self::getMatchTeam1($match, $matchReferential, $teamReferential);
        $team2 = self::getMatchTeam2($match, $matchReferential, $teamReferential);

        if ($match->getWinner() === $team1->getTeamId())
            return $team1;

        if ($match->getWinner() === $team2->getTeamId())
            return $team2;

        return null;
    }
}
