export class PictureUtils {
  public static preparePictureAfterEvent(event: Event, callback: (base64: string, extension: string) => Promise<void>) {
    const input = event.target;
    if (!(input instanceof HTMLInputElement))
      return;

    const file = input.files?.item(0);
    if (!file)
      return;

    const me = this;

    const nameParts = file.name.split('.');
    const extension = nameParts[nameParts.length - 1].toLowerCase();
    if (nameParts.length < 2 || (extension !== 'png' && extension !== 'jpg' && extension !== 'jpeg' && extension !== 'gif')) {
      alert('Invalid file type. Only PNG, JPG and GIF are supported.');
      input.value = '';
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = async function () {
      input.value = '';

      const result = reader.result as string;
      const base64 = result.split(',')[1];

      await callback(base64, extension);
    };
    reader.onerror = function (error) {
      console.error('Error: ', error);
    };
  }
}
