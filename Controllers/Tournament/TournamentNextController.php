<?php

namespace Pickomenka\Controllers\Tournament;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\TournamentDataProvider;

class TournamentNextController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $tournament = TournamentDataProvider::getInstance()->readNextTournament();
        if ($tournament === null)
            $this->notFound();

        echo json_encode($tournament->toJson());
    }
}