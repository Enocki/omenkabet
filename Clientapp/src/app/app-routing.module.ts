import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {authenticationGuard} from "../modules/shared/guards/authentication.guard";
import {adminGuard} from "../modules/shared/guards/admin.guard";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('../modules/welcome/welcome.module').then(m => m.WelcomeModule),
  },
  {
    path: 'admin',
    canActivate: [ adminGuard ],
    loadChildren: () => import('../modules/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'login',
    loadChildren: () => import('../modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'profile',
    canActivate: [ authenticationGuard ],
    loadChildren: () => import('../modules/profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'ranking',
    canActivate: [ authenticationGuard ],
    loadChildren: () => import('../modules/ranking/ranking.module').then(m => m.RankingModule)
  },
  {
    path: 'register',
    loadChildren: () => import('../modules/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'tournament',
    canActivate: [ authenticationGuard ],
    loadChildren: () => import('../modules/tournament/tournament.module').then(m => m.TournamentModule)
  },
  {
    path: 'logout',
    canActivate: [ authenticationGuard ],
    loadChildren: () => import('../modules/logout/logout.module').then(m => m.LogoutModule)
  },
  {
    path: '**',
    loadChildren: () => import('../modules/error404/error404.module').then(m => m.Error404Module)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
