import { NgModule } from '@angular/core';
import { Error404IndexComponent } from './components/error404-index.component';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    Error404IndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: Error404IndexComponent
      }
    ])
  ]
})
export class Error404Module { }
