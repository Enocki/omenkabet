import {Component, Input} from '@angular/core';
import {Team} from "../../models/team";
import {PictureUtils} from "../../models/picture-utils";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.less']
})
export class TeamEditComponent {
  @Input()
  team?: Team;

  constructor(
    private readonly _apiService: ApiService
  ) {}

  get teamName(): string {
    if (this.team === undefined)
      throw new Error('Team is undefined');

    return this.team.teamName;
  }

  set teamName(value: string) {
    if (this.team === undefined)
      return;

    this.team.teamName = value;
  }

  get isNewTeam(): boolean { return this.team?.teamId === undefined; }

  async handleAvatarChange(event: Event) {
    PictureUtils.preparePictureAfterEvent(event, async (base64, extension) => {
      if (!this.team)
        throw new Error('Team is undefined');

      this.team.avatar =
        await this._apiService.updateTeamAvatar(
          this.team.teamId!, base64, extension
        );
    });
  }
}
