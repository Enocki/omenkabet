import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {AuthenticationService} from "../services/authentication.service";

export const adminGuard: CanActivateFn = (route, state) => {
  const authenticationService = inject(AuthenticationService);
  const router = inject(Router);

  return authenticationService.getCurrentUserPromise().then(async user => {
    if (user?.isAdmin ?? false)
      return true;

    await router.navigate(['/'])
    return false;
  });
};
