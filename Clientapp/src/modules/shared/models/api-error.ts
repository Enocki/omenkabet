export class ApiError extends Error {
  public readonly statusCode;
  public readonly details: any;

  constructor(message: string, statusCode: number, details?: any) {
    super(message);
    this.statusCode = statusCode;
    this.details = details;
  }
}
