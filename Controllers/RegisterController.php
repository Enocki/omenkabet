<?php

namespace Pickomenka\Controllers;

use Pickomenka\Database\UserDataProvider;
use Pickomenka\Models\UserModel;
use Pickomenka\Utils\VerifyUtils;

class RegisterController extends AbstractController
{
    private function generateToken(int $length): string {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $token = '';
        for ($i = 0; $i < $length; $i++)
            $token .= $alphabet[rand(0, strlen($alphabet) - 1)];
        return $token;
    }

    public function post(): void
    {
        list($email, $userName, $password) = $this->jsonBody(['email', 'username', 'password']);

        $email = VerifyUtils::verifyString($email, 1, 100);
        $userName = VerifyUtils::verifyString($userName, 1, 45);
        $password = VerifyUtils::verifyString($password, 1, 256);

        //VERIFY EMAIL DOESNT EXISTS
        $existingUser = UserDataProvider::getInstance()->readUserByLogin($email);
        if ($existingUser !== null)
            $this->badRequest('Email is already used.');

        //VERIFY USERNAME DOESNT EXISTS
        $existingUser = UserDataProvider::getInstance()->readUserByLogin($userName);
        if ($existingUser !== null)
            $this->badRequest('Username is already used.');

        //VERIFY TOKEN DOESNT EXISTS
        do {
            $authToken = $this->generateToken(50);
            $existingUser = UserDataProvider::getInstance()->readUserByToken($authToken);
        }
        while($existingUser !== null);

        //CREATE
        $user = new UserModel(0, $authToken, null, $email, password_hash($password, PASSWORD_DEFAULT), 0, $userName);
        UserDataProvider::getInstance()->createUser($user);

        $_SESSION['userid'] = $user->getUserId();
        $this->updateAuthCookies($user->getAuthToken());
        echo json_encode($user->toJson());
    }
}
