START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE2 j1');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('4', '7', '1', '4');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('10', '3', '1', '4');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('5', '8', '1', '4');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('2', '6', '1', '4');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('9', '1', '1', '4');

COMMIT;


START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE2 j2');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('10', '7', '1', '5');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('9', '2', '1', '5');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('3', '5', '1', '5');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('6', '4', '1', '5');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '8', '1', '5');

COMMIT;

START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE2 j3');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('7', '3', '1', '6');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '10', '1', '6');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('6', '9', '1', '6');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('5', '4', '1', '6');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('8', '2', '1', '6');

COMMIT;







START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE3 j1');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('9', '4', '1', '7');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '3', '1', '7');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('2', '5', '1', '7');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('10', '6', '1', '7');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('8', '7', '1', '7');

COMMIT;


START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE3 j2');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('10', '9', '1', '8');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('7', '2', '1', '8');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('6', '5', '1', '8');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('3', '8', '1', '8');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('4', '1', '1', '8');

COMMIT;


START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE3 j3');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('7', '1', '1', '9');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('2', '10', '1', '9');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('5', '9', '1', '9');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('3', '6', '1', '9');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('4', '8', '1', '9');

COMMIT;


