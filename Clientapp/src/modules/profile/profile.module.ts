import { NgModule } from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import { ProfileIndexComponent } from './components/profile-index.component';



@NgModule({
  declarations: [
    ProfileIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProfileIndexComponent
      }
    ])
  ]
})
export class ProfileModule { }
