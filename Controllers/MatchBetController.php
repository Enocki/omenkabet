<?php

namespace Pickomenka\Controllers;

use DateTime;
use Pickomenka\Database\MatchBetDataProvider;
use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Models\MatchBetModel;
use Pickomenka\Utils\VerifyUtils;

class MatchBetController extends AbstractController
{
    private function verifyMatchBet(MatchBetModel $matchBet): void
    {
        $match = MatchDataProvider::getInstance()->readMatch($matchBet->getMatchId());
        if ($match === null)
            $this->badRequest('Match does not exist.');

        if ($match->getWinner() !== null)
            $this->badRequest('Match is already determined.');

        $tournament = TournamentDataProvider::getInstance()->readTournament($match->getTournamentId());
        if ($tournament->getStartDate() !== null && $tournament->getStartDate()->getTimestamp() < (new DateTime())->getTimestamp())
            $this->badRequest('Tournament has already started.');

        $team = TeamDataProvider::getInstance()->readTeam($matchBet->getTeamId());
        if ($team === null)
            $this->badRequest('Team does not exist.');

        $existingMatchBet = MatchBetDataProvider::getInstance()->readMatchbetByUseridAndMatchid($this->getLoggedUserId(), $match->getMatchId());
        if ($existingMatchBet !== null && $existingMatchBet->getMatchBetId() !== $matchBet->getMatchBetId())
            $this->badRequest('You already bet on this match');
    }

    public function post(): void
    {
        $this->ensureAuthentication();

        $matchBet = MatchBetModel::fromJson($this->jsonBody(), $this->getLoggedUserId());

        $this->verifyMatchbet($matchBet);
        MatchBetDataProvider::getInstance()->createMatchBet($matchBet);

        echo json_encode($matchBet->toJson());
    }

    public function put(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $matchBet = MatchBetModel::fromJson($this->jsonBody(), $this->getLoggedUserId());
        if ($matchBet->getMatchBetId() !== $id)
            $this->badRequest('Id mismatch.');

        $existingMatchBet = MatchBetDataProvider::getInstance()->readMatchBet($id);
        if ($existingMatchBet === null)
            $this->notFound();

        if ($existingMatchBet->getUserId() !== $this->getLoggedUserId())
            $this->forbidden();

        $this->verifyMatchBet($matchBet);
        MatchBetDataProvider::getInstance()->updateMatchBet($matchBet);

        echo json_encode($matchBet->toJson());
    }

    public function delete(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $matchBet = MatchBetDataProvider::getInstance()->readMatchBet($id);
        if ($matchBet === null)
            $this->notFound();

        if ($matchBet->getUserId() !== $this->getLoggedUserId())
            $this->forbidden();

        $this->verifyMatchBet($matchBet);
        MatchBetDataProvider::getInstance()->deleteMatchBet($id);

        echo json_encode($matchBet->toJson());
    }
}