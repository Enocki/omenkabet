<?php

namespace Pickomenka\Utils;

trait SingletonTrait
{
    static private mixed $instance;

    public static function getInstance(): static {
        if (!isset(self::$instance))
            self::$instance = new static();

        return self::$instance;
    }

    abstract protected function __construct();
}