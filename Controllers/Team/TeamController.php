<?php

namespace Pickomenka\Controllers\Team;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Models\TeamModel;
use Pickomenka\Utils\VerifyUtils;

class TeamController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $team = TeamDataProvider::getInstance()->readTeam($id);
        if ($team === null)
            $this->notFound();

        echo json_encode($team->toJson());
    }

    public function post(): void
    {
        $this->ensureAdmin();

        $team = TeamModel::fromJson($this->jsonBody());
        TeamDataProvider::getInstance()->createTeam($team);

        echo json_encode($team->toJson());
    }

    public function put(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $team = TeamModel::fromJson($this->jsonBody());
        if ($team->getTeamId() !== $id)
            $this->badRequest('Id mismatch.');

        $existingTeam = TeamDataProvider::getInstance()->readTeam($id);
        if ($existingTeam === null)
            $this->notFound();

        TeamDataProvider::getInstance()->updateTeam($team);

        echo json_encode($team->toJson());
    }

    public function delete(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $team = TeamDataProvider::getInstance()->readTeam($id);
        if ($team === null)
            $this->notFound();

        TeamDataProvider::getInstance()->deleteTeam($id);
    }
}
