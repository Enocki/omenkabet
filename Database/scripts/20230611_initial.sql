START TRANSACTION;

CREATE TABLE `users` (
    `userid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `authtoken` VARCHAR(100) NULL DEFAULT NULL,
    `avatar` VARCHAR(45) NULL DEFAULT NULL,
    `email` VARCHAR(100) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `role` INT UNSIGNED NOT NULL DEFAULT 0,
    `username` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`userid`),
    UNIQUE INDEX `email_UNIQUE` (`email` ASC),
    UNIQUE INDEX `username_UNIQUE` (`username` ASC),
    UNIQUE INDEX `authtoken_UNIQUE` (`authtoken` ASC)) ENGINE = InnoDB;


CREATE TABLE `tournaments` (
    `tournamentid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `tournamentname` VARCHAR(45) NOT NULL,
    `startdate` BIGINT NULL,
    `enddate` BIGINT NULL,
    PRIMARY KEY (`tournamentid`),
    UNIQUE INDEX `tournamentname_UNIQUE` (`tournamentname` ASC)) ENGINE = InnoDB;


CREATE TABLE `teams` (
    `teamid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `teamname` VARCHAR(45) NOT NULL,
    `avatar` VARCHAR(45) NULL,
    PRIMARY KEY (`teamid`),
    UNIQUE INDEX `teamid_UNIQUE` (`teamid` ASC)) ENGINE = InnoDB;


CREATE TABLE `matchs` (
    `matchid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `parentmatch1` BIGINT UNSIGNED NULL,
    `parentmatch2` BIGINT UNSIGNED NULL,
    `team1` BIGINT UNSIGNED NULL,
    `team2` BIGINT UNSIGNED NULL,
    `winner` BIGINT UNSIGNED NULL,
    `startdate` BIGINT UNSIGNED NULL,
    `matchtype` TINYINT UNSIGNED NOT NULL,
    `loserscore` INT UNSIGNED NULL,
    `tournamentid` BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (`matchid`),
    UNIQUE INDEX `matchid_UNIQUE` (`matchid` ASC)) ENGINE = InnoDB;


ALTER TABLE `matchs`
    ADD INDEX `fk_matchs_parent1_idx` (`parentmatch1` ASC),
    ADD INDEX `fk_matchs_parent2_idx` (`parentmatch2` ASC),
    ADD INDEX `fk_matchs_team1_idx` (`team1` ASC),
    ADD INDEX `fk_matchs_team2_idx` (`team2` ASC),
    ADD INDEX `fk_matchs_tournament_idx` (`tournamentid` ASC);

ALTER TABLE `matchs`
ADD CONSTRAINT `fk_matchs_parent1`
    FOREIGN KEY (`parentmatch1`)
    REFERENCES `matchs` (`matchid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_matchs_parent2`
  FOREIGN KEY (`parentmatch2`)
  REFERENCES `matchs` (`matchid`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_matchs_team1`
    FOREIGN KEY (`team1`)
    REFERENCES `teams` (`teamid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_matchs_team2`
    FOREIGN KEY (`team2`)
    REFERENCES `teams` (`teamid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_matchs_tournament`
    FOREIGN KEY (`tournamentid`)
    REFERENCES `tournaments` (`tournamentid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


CREATE TABLE `matchbets` (
    `matchbetid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user` BIGINT UNSIGNED NOT NULL,
    `team` BIGINT UNSIGNED NOT NULL,
    `match` BIGINT UNSIGNED NOT NULL,
    `loserscore` INT UNSIGNED NULL,
    PRIMARY KEY (`matchbetid`),
    UNIQUE INDEX `betid_UNIQUE` (`matchbetid` ASC));

CREATE TABLE `tournamentbets` (
    `tournamentbetid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `tournament` BIGINT UNSIGNED NOT NULL,
    `team` BIGINT UNSIGNED NOT NULL,
    `user` BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (`tournamentbetid`),
    UNIQUE INDEX `tournament_UNIQUE` (`tournament` ASC),
    UNIQUE INDEX `tournamentbetid_UNIQUE` (`tournamentbetid` ASC));


ALTER TABLE `matchbets`
    ADD INDEX `fk_matchbets_user_idx` (`user` ASC),
    ADD INDEX `fk_matchbets_team_idx` (`team` ASC),
    ADD INDEX `fk_matchbets_match_idx` (`match` ASC);
;
ALTER TABLE `matchbets`
    ADD CONSTRAINT `fk_matchbets_user`
    FOREIGN KEY (`user`)
    REFERENCES `users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    ADD CONSTRAINT `fk_matchbets_team`
    FOREIGN KEY (`team`)
    REFERENCES `teams` (`teamid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    ADD CONSTRAINT `fk_matchbets_match`
    FOREIGN KEY (`match`)
    REFERENCES `matchs` (`matchid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


ALTER TABLE `tournamentbets`
    ADD INDEX `fk_tournamentbetid_team_idx` (`team` ASC),
    ADD INDEX `fk_tournamentbet_tournament_idx` (`tournament` ASC),
    ADD INDEX `fk_tournamentbet_user_idx` (`user` ASC);
;
ALTER TABLE `tournamentbets`
    ADD CONSTRAINT `fk_tournamentbet_team`
    FOREIGN KEY (`team`)
    REFERENCES `teams` (`teamid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    ADD CONSTRAINT `fk_tournamentbet_user`
    FOREIGN KEY (`user`)
    REFERENCES `users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    ADD CONSTRAINT `fk_tournamentbet_tournament`
    FOREIGN KEY (`tournament`)
    REFERENCES `tournaments` (`tournamentid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

COMMIT;