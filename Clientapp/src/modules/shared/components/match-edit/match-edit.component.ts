import {Component, Input} from '@angular/core';
import {EMatchType, Match} from "../../models/match";
import {Team} from "../../models/team";
import {MatchService} from "../../services/match.service";

@Component({
  selector: 'match-edit',
  templateUrl: './match-edit.component.html',
  host: {
    class: 'flex-column gap-3'
  }
})
export class MatchEditComponent {
  private _match?: Match;
  @Input()
  set match(value: Match) {
    this._match = value;
  }

  @Input()
  teams: Team[] = [];

  @Input()
  matches: Match[] = [];

  constructor(
    private readonly _matchService: MatchService
  ) {}

  matchTypes: { value: EMatchType, label: string }[] = [
    { value: EMatchType.BO1, label: 'BO1' },
    { value: EMatchType.BO3, label: 'BO3' },
    { value: EMatchType.BO5, label: 'BO5' },
  ];

  get matchType(): EMatchType | undefined {
    return this._match?.matchType;
  }

  set matchType(value: EMatchType | undefined) {
    if (this._match === undefined || value === undefined)
      return;

    this._match.matchType = value;
  }

  get team1(): Team | undefined {
    if (this._match === undefined)
      return undefined;

    return this._matchService.getMatchTeam1(this._match, this.matches, this.teams);
  }

  set team1(value: Team | undefined) {
    if (this._match === undefined)
      return;

    this._match.teamId1 = value?.teamId;
  }

  get team2(): Team | undefined {
    if (this._match === undefined)
      return undefined;

    return this._matchService.getMatchTeam2(this._match, this.matches, this.teams);
  }

  set team2(value: Team | undefined) {
    if (this._match === undefined)
      return;

    this._match.teamId2 = value?.teamId;
  }

  get startDate(): string {
    let startDate = this._match?.startDate;
    if (startDate === undefined)
      return '';

    startDate = new Date(startDate);
    startDate.setTime(startDate.getTime() - startDate.getTimezoneOffset() * 60 * 1000);

    return startDate.toISOString().slice(0, 16);
  }

  set startDate(value: string) {
    if (this._match === undefined)
      return;

    this._match.startDate = new Date(value);
  }

  get winner(): 'team1' | 'team2' | 'no-one' {
    if (this._match === undefined || this._match.winner === undefined)
      return 'no-one';

    if (this._match.winner === this._match.teamId1)
      return 'team1';

    if (this._match.winner === this._match.teamId2)
      return 'team2';

    return 'no-one';
  }

  set winner(value: 'team1' | 'team2' | 'no-one') {
    if (this._match === undefined)
      return;

    if (value === 'no-one') {
      this._match.winner = undefined;
      return;
    }

    if (value === 'team1') {
      this._match.winner = this._match.teamId1;
      return;
    }

    if (value === 'team2') {
      this._match.winner = this._match.teamId2;
      return;
    }
  }

  get loserScore(): number | undefined {
    return this._match?.loserScore;
  }

  set loserScore(value: number | undefined) {
    if (this._match === undefined)
      return;

    this._match.loserScore = value;
  }
}
