import { Injectable } from '@angular/core';
import {ApiService} from "./api.service";
import {Observable, Subject} from "rxjs";
import {User} from "../models/user";
import {ApiError} from "../models/api-error";
import {SettingsService} from "./settings.service";
import {CdnUrlPipe} from "../pipes/cdn-url.pipe";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _currentUser?: User;
  private _currentUserPromise?: Promise<User | undefined>;
  private _currentUserSubject: Subject<User> = new Subject<User>();

  constructor(
    private readonly _apiService: ApiService
  ) {}

  public get currentUserObservable(): Observable<User> {
    return this._currentUserSubject.asObservable();
  }

  public get currentUser(): User | undefined {
    return this._currentUser;
  }

  public async getCurrentUserPromise(): Promise<User | undefined> {
    if (this._currentUserPromise !== undefined)
      await this._currentUserPromise;
    else
      await this.refreshCurrentUser();

    return this._currentUser;
  }

  public get isAuthenticated(): boolean {
    return this._currentUser !== undefined;
  }

  public get isAdmin(): boolean {
    return this._currentUser?.isAdmin ?? false;
  }

  private async _handleUserRequest(request: Promise<User>, throwError: boolean): Promise<User | undefined> {
    let _resolve: (value: (User | PromiseLike<User | undefined> | undefined)) => void;
    this._currentUserPromise = new Promise<User | undefined>(resolve => _resolve = resolve);

    try {
      const loggedUser = await request;
      this._currentUser = loggedUser;
      this._currentUserSubject.next(loggedUser);

      _resolve!(loggedUser);
      return loggedUser;
    }
    catch (e: any) {
      _resolve!(undefined);

      if (!throwError && e instanceof ApiError)
        if (e.statusCode === 401)
          return undefined;

      throw e;
    }
  }

  public async refreshCurrentUser(): Promise<User | undefined> {
    return await this._handleUserRequest(this._apiService.readUser(), false);
  }

  public async login(login: string, password: string): Promise<User | undefined> {
    return await this._handleUserRequest(this._apiService.login(login, password), true);
  }

  public async register(email: string, username: string, password: string) {
    return await this._handleUserRequest(this._apiService.register(email, username, password), true);
  }

  public async logout(): Promise<void> {
    await this._apiService.logout();
    this._currentUser = undefined;
  }
}
