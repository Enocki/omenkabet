import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../shared/services/api.service";
import {Tournament} from "../../shared/models/tournament";

@Component({
  selector: 'tournament-index',
  templateUrl: './tournament-index.component.html',
  styleUrls: ['./tournament-index.component.less']
})
export class TournamentIndexComponent implements OnInit {

  public tournaments: Tournament[] = [];

  constructor(private apiService: ApiService) {
  }

  async ngOnInit(): Promise<void> {
    this.tournaments = await this.apiService.readTournaments();
  }
}
