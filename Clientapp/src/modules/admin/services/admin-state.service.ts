import { Injectable } from '@angular/core';
import {ApiService} from "../../shared/services/api.service";
import {Tournament} from "../../shared/models/tournament";
import {Observable, Subject} from "rxjs";
import {Team} from "../../shared/models/team";

@Injectable({
  providedIn: 'root'
})
export class AdminStateService {

  private _tournaments: Tournament[] = [];
  private _teams: Team[] = [];

  private _tournament?: Tournament;
  private _tournamentChangeSubject: Subject<Tournament | undefined> = new Subject<Tournament | undefined>();

  constructor(
    private readonly _apiService: ApiService
  ) { }

  get tournamentChangeObservable(): Observable<Tournament | undefined> { return this._tournamentChangeSubject.asObservable(); }

  get tournament(): Tournament | undefined { return this._tournament; }

  set tournament(value: Tournament | undefined) {
    this._tournament = value;
    this._tournamentChangeSubject.next(value);
  }

  get tournaments(): Tournament[] { return this._tournaments; }
  async refreshTournaments(): Promise<void> {
    this._tournaments = await this._apiService.readTournaments(true);
  }

  get teams(): Team[] { return this._teams; }
  async refreshTeams(): Promise<void> {
    this._teams = await this._apiService.readTeams();
  }
}
