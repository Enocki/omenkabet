export class ColorHelper {

  static Darken(rgb: RGB, amount: number) {
    const hsl = this.RGBToHSL(rgb);

    hsl.l = Math.min(100, Math.max(0, hsl.l - amount));

    return ColorHelper.HSLToRGB(hsl);
  }

  static Lighten(rgb: RGB, amount: number): RGB {
    const hsl = this.RGBToHSL(rgb);

    hsl.l = Math.min(100, Math.max(0, hsl.l + amount));

    return ColorHelper.HSLToRGB(hsl);
  }

  static Saturate(rgb: RGB, amount: number): RGB {
    const hsl = this.RGBToHSL(rgb);

    hsl.s = Math.min(100, Math.max(0, hsl.s + amount));

    return ColorHelper.HSLToRGB(hsl);
  }

  static HexToRGB(hex: string): RGB {
    let r = 0, g = 0, b = 0;

    if (hex === undefined) return { r: r, g: g, b: b };

    // 3 digits
    if (hex.length == 4) {
      r = parseInt(hex[1] + hex[1], 16);
      g = parseInt(hex[2] + hex[2], 16);
      b = parseInt(hex[3] + hex[3], 16);

      // 6 digits
    } else if (hex.length == 7) {
      r = parseInt(hex[1] + hex[2], 16);
      g = parseInt(hex[3] + hex[4], 16);
      b = parseInt(hex[5] + hex[6], 16);
    }

    return { r: r, g: g, b: b };
  }

  static HSLToRGB(hsl: HSL): RGB {
    // Must be fractions of 1
    let s = hsl.s / 100;
    let l = hsl.l / 100;
    let h = hsl.h;

    let c = (1 - Math.abs(2 * l - 1)) * s,
      x = c * (1 - Math.abs((h / 60) % 2 - 1)),
      m = l - c / 2,
      r = 0,
      g = 0,
      b = 0;

    if (0 <= h && h < 60) {
      r = c; g = x; b = 0;
    } else if (60 <= h && h < 120) {
      r = x; g = c; b = 0;
    } else if (120 <= h && h < 180) {
      r = 0; g = c; b = x;
    } else if (180 <= h && h < 240) {
      r = 0; g = x; b = c;
    } else if (240 <= h && h < 300) {
      r = x; g = 0; b = c;
    } else if (300 <= h && h < 360) {
      r = c; g = 0; b = x;
    }
    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return { r: r, g: g, b: b };
  }

  static RGBToHex(rgb: RGB): string {
    let r = rgb.r.toString(16);
    let g = rgb.g.toString(16);
    let b = rgb.b.toString(16);

    if (r.length == 1)
      r = "0" + r;
    if (g.length == 1)
      g = "0" + g;
    if (b.length == 1)
      b = "0" + b;

    return "#" + r + g + b;
  }

  static RGBToHSL(rgb: RGB): HSL {
    // Make r, g, and b fractions of 1
    let r = rgb.r / 255;
    let g = rgb.g / 255;
    let b = rgb.b / 255;

    // Find greatest and smallest channel values
    let cmin = Math.min(r, g, b),
      cmax = Math.max(r, g, b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;

    // Calculate hue
    // No difference
    if (delta == 0)
      h = 0;
    // Red is max
    else if (cmax == r)
      h = ((g - b) / delta) % 6;
    // Green is max
    else if (cmax == g)
      h = (b - r) / delta + 2;
    // Blue is max
    else
      h = (r - g) / delta + 4;

    h = Math.round(h * 60);

    // Make negative hues positive behind 360°
    if (h < 0)
      h += 360;

    // Calculate lightness
    l = (cmax + cmin) / 2;

    // Calculate saturation
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

    // Multiply l and s by 100
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return { h: h, s: s, l: l }
  }
}

export interface HSL { h: number; s: number; l: number; }
export interface RGB { r: number; g: number; b: number; }
