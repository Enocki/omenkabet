<?php

namespace Pickomenka\Models;

use Pickomenka\Utils\ArrayUtils;
use Pickomenka\Utils\VerifyUtils;

class TournamentBetModel
{

    private ?int $tournamentBetId;
    private int $team;
    private int $user;
    private int $tournament;

    public function __construct(?int $tournamentBetId, int $team, int $user, int $tournament) {
        $this->tournamentBetId = $tournamentBetId;
        $this->team = $team;
        $this->user = $user;
        $this->tournament = $tournament;
    }

    /**
     * @return int | null
     */
    public function getTournamentBetId(): int | null
    {
        return $this->tournamentBetId;
    }

    /**
     * @param int $tournamentBetId
     */
    public function setTournamentBetId(int $tournamentBetId): void
    {
        $this->tournamentBetId = $tournamentBetId;
    }

    /**
     * @return int
     */
    public function getTeam(): int
    {
        return $this->team;
    }

    /**
     * @param int $team
     */
    public function setTeam(int $team): void
    {
        $this->team = $team;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getTournament(): int
    {
        return $this->tournament;
    }

    /**
     * @param int $tournament
     */
    public function setTournament(int $tournament): void
    {
        $this->tournament = $tournament;
    }

    /**
     * @return int[]
     */
    public function toJson(): array
    {
        return [
            'tournamentBetId' => $this->getTournamentBetId(),
            'teamId' => $this->getTeam(),
            'userId' => $this->getUser(),
            'tournamentId' => $this->getTournament()
        ];
    }

    public static function fromJson(array $json, int $userId): TournamentBetModel
    {
        list($team, $tournament) = ArrayUtils::checkArrayFields($json, ['teamId', 'tournamentId'], ['tournamentBetId']);

        $team = VerifyUtils::verifyNumber($team);
        $tournament = VerifyUtils::verifyNumber($tournament);

        return new TournamentBetModel($tournamentBetId ?? 0, $team, $userId, $tournament);
    }
}
