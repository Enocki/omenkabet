<?php

namespace Pickomenka\Controllers\Tournament;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Utils\CdnUtils;
use Pickomenka\Utils\VerifyUtils;

class TournamentAvatarController extends AbstractController
{
    public function put(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $existingTournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($existingTournament === null)
            $this->notFound();

        list($avatar, $avatarExtension) = $this->jsonBody(['avatar', 'extension']);

        if (!in_array($avatarExtension, CdnUtils::getInstance()->getAllowedAvatarExtensions()))
            $this->badRequest('Invalid avatar extension.');

        if ($existingTournament->getAvatar() !== null)
            CdnUtils::getInstance()->deleteFromCdn($existingTournament->getAvatar());

        $newAvatar = CdnUtils::getInstance()->postToCdn(CdnUtils::getInstance()->getMimeTypeByExtension()[$avatarExtension], $avatar);
        $existingTournament->setAvatar($newAvatar);

        TournamentDataProvider::getInstance()->updateTournament($existingTournament, true);

        echo json_encode([ 'avatar' => $newAvatar ]);
    }
}