import { Injectable } from '@angular/core';
import {Team} from "../models/team";
import {Match} from "../models/match";

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor() { }


  private _getTeam(teamId: number, teams: Team[]): Team | undefined {
    return teams.find(t => t.teamId === teamId);
  }

  public getMatchWinner(match: Match, matches: Match[], teams: Team[]): Team | undefined {
    const team1 = this.getMatchTeam1(match, matches, teams);
    const team2 = this.getMatchTeam1(match, matches, teams);

    if (match.winner === match.teamId1)
      return team1;

    if (match.winner === match.teamId2)
      return team2;

    return undefined;
  }

  public getMatchTeam1(match: Match, matches: Match[], teams: Team[]): Team | undefined {
    if (!match.isMatchDeterminedByParents)
      return this._getTeam(match.teamId1!, teams);

    const parentMatch = matches.find(m => m.matchId === match.parentMatchId1);
    if (parentMatch?.winner !== undefined)
      return this.getMatchWinner(parentMatch, matches, teams);

    return undefined;
  }

  public getMatchTeam2(match: Match, matches: Match[], teams: Team[]): Team | undefined {
    if (!match.isMatchDeterminedByParents)
      return this._getTeam(match.teamId2!, teams);

    const parentMatch = matches.find(m => m.matchId === match.parentMatchId2);
    if (parentMatch?.winner !== undefined)
      return this.getMatchWinner(parentMatch, matches, teams);

    return undefined;
  }
}
