<?php

namespace Pickomenka\Database;

use Pickomenka\Models\UserModel;
use Pickomenka\Utils\SingletonTrait;

class UserDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    /**
     * Create a new user
     * @param UserModel $user
     * @return void
     */
    public function createUser(UserModel $user): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO users (authtoken, avatar, email, password, role, username) VALUES (?, ?, ?, ?, ?, ?)',
            [$user->getAuthToken(), $user->getAvatar(), $user->getEmail(), $user->getPassword(), $user->getRole(), $user->getUserName()]
        );

        $user->setUserId($result->lastInsertId);
    }

    /**
     * Get user by id
     * @param int $userId
     * @return UserModel | null
     */
    public function readUser(int $userId): UserModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT userid, authtoken, avatar, email, password, role, username FROM users WHERE userid = ?',
            [$userId]
        );

        if ($rs === false || $rs === null)
            return null;

        return new UserModel(
            $rs['userid'],
            $rs['authtoken'],
            $rs['avatar'],
            $rs['email'],
            $rs['password'],
            $rs['role'],
            $rs['username']
        );
    }

    public function readUserByToken(string $authToken): UserModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT userid, authtoken, avatar, email, password, role, username FROM users WHERE BINARY authtoken = ?',
            [$authToken]
        );

        if ($rs === false || $rs === null)
            return null;

        return new UserModel(
            $rs['userid'],
            $rs['authtoken'],
            $rs['avatar'],
            $rs['email'],
            $rs['password'],
            $rs['role'],
            $rs['username']
        );
    }

    /**
     * @param string $login
     * @param string $password
     * @return UserModel | null
     */
    public function readUserByLogin(string $login): UserModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT userid, authtoken, avatar, email, password, role, username FROM users 
                    WHERE (username = ? OR email = ?)',
            [$login, $login]
        );

        return new UserModel(
            $rs['userid'],
            $rs['authtoken'],
            $rs['avatar'],
            $rs['email'],
            $rs['password'],
            $rs['role'],
            $rs['username']
        );
    }

    /**
     * @param array $userIds
     * @return UserModel[]
     */
    public function readUsers(array $userIds): array {
        $rs = Database::getInstance()->queryAll(
            'SELECT userid, authtoken, avatar, email, password, role, username FROM users WHERE userid IN (' . implode(',', $userIds) . ')',
            []
        );

        $users = [];
        foreach ($rs as $row) {
            $users[] = new UserModel(
                $row['userid'],
                $row['authtoken'],
                $row['avatar'],
                $row['email'],
                $row['password'],
                $row['role'],
                $row['username']
            );
        }

        return $users;
    }

    /**
     * @param UserModel $user
     * @return void
     */
    public function updateUser(UserModel $user): void
    {
        Database::getInstance()->update(
            'UPDATE users SET authtoken = ?, avatar = ?, email = ?, password = ?, role = ?, username = ? WHERE userid = ?',
            [$user->getAuthToken(), $user->getAvatar(), $user->getEmail(), $user->getPassword(), $user->getRole(), $user->getUserName(), $user->getUserId()]
        );
    }
}