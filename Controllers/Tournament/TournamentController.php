<?php

namespace Pickomenka\Controllers\Tournament;

use JetBrains\PhpStorm\NoReturn;
use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\MatchBetDataProvider;
use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Models\MatchBetModel;
use Pickomenka\Models\MatchModel;
use Pickomenka\Models\TeamModel;
use Pickomenka\Models\TournamentModel;
use Pickomenka\Utils\VerifyUtils;

class TournamentController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($tournament === null)
            $this->notFound();

        $teams = TeamDataProvider::getInstance()->readTeamsByTournament($id);
        $matches = MatchDataProvider::getInstance()->readMatchesByTournament($id);
        $matchBets = MatchBetDataProvider::getInstance()->readMatchBetsByTournament($id);

        $viewTournament = $tournament->toJson();
        $viewTournament['matches'] = array_map(function (MatchModel $match) {
            return $match->toJson();
        }, $matches);

        $viewTournament['teams'] = array_map(function (TeamModel $team) {
            return $team->toJson();
        }, $teams);

        $viewTournament['matchBets'] = array_map(function (MatchBetModel $matchBet) {
            return $matchBet->toJson();
        }, $matchBets);

        $viewTournament['previousTournamentId'] = TournamentDataProvider::getInstance()->readPreviousTournamentId($tournament);
        $viewTournament['nextTournamentId'] = TournamentDataProvider::getInstance()->readNextTournamentId($tournament);

        echo json_encode($viewTournament);
    }

    public function post(): void
    {
        $this->ensureAdmin();

        $tournament = TournamentModel::fromJson($this->jsonBody());
        TournamentDataProvider::getInstance()->createTournament($tournament);

        echo json_encode($tournament->toJson());
    }

    public function put(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $existingTournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($existingTournament === null)
            $this->notFound();

        $tournament = TournamentModel::fromJson($this->jsonBody());
        if ($tournament->getTournamentId() !== $id)
            $this->badRequest('Id mismatch.');

        TournamentDataProvider::getInstance()->updateTournament($tournament, false);

        echo json_encode($tournament->toJson());
    }

    #[NoReturn] public function delete(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($tournament === null)
            $this->notFound();

        TournamentDataProvider::getInstance()->deleteTournament($id);
        $this->noContent();
    }
}
