<?php

namespace Pickomenka\Models;

use DateTime;
use Pickomenka\Utils\ArrayUtils;
use Pickomenka\Utils\VerifyUtils;

class TournamentModel
{
    private int $tournamentId;
    private string $tournamentName;
    private ?DateTime $startDate;
    private ?DateTime $endDate;
    private ?string $avatar;

    public function __construct(int $tournamentId, string $tournamentName, ?DateTime $startDate, ?DateTime $endDate, ?string $avatar)
    {
        $this->tournamentId = $tournamentId;
        $this->tournamentName = $tournamentName;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->avatar = $avatar;
    }

    /**
     * @return int
     */
    public function getTournamentId(): int
    {
        return $this->tournamentId;
    }

    /**
     * @param int $tournamentId
     */
    public function setTournamentId(int $tournamentId): void
    {
        $this->tournamentId = $tournamentId;
    }

    /**
     * @return string
     */
    public function getTournamentName(): string
    {
        return $this->tournamentName;
    }

    /**
     * @param string $tournamentName
     */
    public function setTournamentName(string $tournamentName): void
    {
        $this->tournamentName = $tournamentName;
    }

    /**
     * @return DateTime|null
     */
    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime|null $startDate
     */
    public function setStartDate(?DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime|null
     */
    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime|null $endDate
     */
    public function setEndDate(?DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(?string $avatar): void
    {
        $this->avatar = $avatar;
    }

    public function toJson(): array
    {
        return [
            'avatar' => $this->getAvatar(),
            'tournamentId' => $this->getTournamentId(),
            'tournamentName' => $this->getTournamentName(),
            'startDate' => $this->getStartDate()?->getTimestamp(),
            'endDate' => $this->getEndDate()?->getTimestamp()
        ];
    }

    public static function fromJson(array $json): TournamentModel
    {
        list($tournamentName, $tournamentId, $startDate, $endDate) = ArrayUtils::checkArrayFields($json, ['tournamentName'], ['tournamentId', 'startDate', 'endDate']);

        $tournamentName = VerifyUtils::verifyString($tournamentName, 0, 45);
        $startDate = VerifyUtils::verifyNumber($startDate, allowNull: true);
        $endDate = VerifyUtils::verifyNumber($endDate, allowNull: true);
        $tournamentId = VerifyUtils::verifyNumber($tournamentId, allowNull: true);

        $startDate = $startDate === null ? null : DateTime::createFromFormat('U', $startDate);
        $endDate = $endDate === null ? null : DateTime::createFromFormat('U', $endDate);

        return new TournamentModel($tournamentId ?? 0, $tournamentName, $startDate, $endDate, null);
    }
}