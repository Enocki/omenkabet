import { Pipe, PipeTransform } from '@angular/core';
import {SettingsService} from "../services/settings.service";

@Pipe({
  name: 'cdnUrl'
})
export class CdnUrlPipe implements PipeTransform {

  constructor(private readonly _settingsService: SettingsService) {}

  transform(value: string | undefined, fallback: string | undefined = undefined): string | undefined {
    return !!value ? this._settingsService.CDN_URL + value : fallback;
  }
}
