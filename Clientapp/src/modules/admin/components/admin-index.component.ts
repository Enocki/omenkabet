import {Component, OnInit} from '@angular/core';
import {AdminStateService} from "../services/admin-state.service";

export type AdminTabs = 'teams' | 'tournaments' | 'users';

@Component({
  selector: 'admin-index',
  templateUrl: './admin-index.component.html',
  host: {
    class: 'w-100 flex-column p-5'
  }
})
export class AdminIndexComponent implements OnInit {

  selectedTab: AdminTabs = 'tournaments';

  constructor(
    private readonly _stateService: AdminStateService
  ) { }

  async ngOnInit(): Promise<void> {
    await this._stateService.refreshTournaments();
    await this._stateService.refreshTeams();
  }

}
