export class User {

  public userId: number;
  public avatar?: string;
  public email: string;
  public isAdmin: boolean;
  public userName: string;

  constructor(data: any) {
    this.userId = data['userId'];
    this.avatar = data['avatar'];
    this.email = data['email'];
    this.isAdmin = data['isAdmin'] ?? undefined;
    this.userName = data['userName'];
  }

  public toJson(): any {
    return {
      userId: this.userId,
      avatar: this.avatar,
      email: this.email,
      isAdmin: this.isAdmin,
      userName: this.userName
    };
  }
}
