export enum EMatchType {
  BO1 = 1,
  BO3 = 3,
  BO5 = 5
}

export interface IMatch {
  matchId?: number;
  parentMatchId1?: number;
  parentMatchId2?: number;
  teamId1?: number;
  teamId2?: number;
  winner?: number;
  startDate?: number;
  matchType: EMatchType;
  loserScore?: number;
  tournamentId: number;
  isMatchDeterminedByParents: boolean;
}

export class Match {
  public matchId?: number;
  public parentMatchId1?: number;
  public parentMatchId2?: number;
  public teamId1?: number;
  public teamId2?: number;
  public winner?: number;
  public startDate?: Date;
  public matchType: EMatchType;
  public loserScore?: number;
  public tournamentId: number;
  public isMatchDeterminedByParents: boolean;

  constructor(data: IMatch) {
    this.matchId = data['matchId'];
    this.parentMatchId1 = data['parentMatchId1'];
    this.parentMatchId2 = data['parentMatchId2'];
    this.teamId1 = data['teamId1'];
    this.teamId2 = data['teamId2'];
    this.winner = data['winner'];
    this.startDate = data['startDate'] ? new Date(data['startDate'] * 1000) : undefined;
    this.matchType = data['matchType'];
    this.loserScore = data['loserScore'];
    this.tournamentId = data['tournamentId'];
    this.isMatchDeterminedByParents = data['isMatchDeterminedByParents'];
  }

  public toJson(): any {
    return {
      matchId: this.matchId,
      parentMatchId1: this.parentMatchId1,
      parentMatchId2: this.parentMatchId2,
      teamId1: this.teamId1,
      teamId2: this.teamId2,
      winner: this.winner,
      startDate: !!this.startDate ? this.startDate.getTime() / 1000 : undefined,
      matchType: this.matchType,
      loserScore: this.loserScore,
      tournamentId: this.tournamentId,
      isMatchDeterminedByParents: this.isMatchDeterminedByParents
    };
  }
}
