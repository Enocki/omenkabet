<?php

namespace Pickomenka\Database;

readonly class DatabaseUpdateResult
{
    public int $affectedRows;
    public mixed $lastInsertId;

    public function __construct(int $affectedRows, mixed $lastInsertId)
    {
        $this->affectedRows = $affectedRows;
        $this->lastInsertId = $lastInsertId;
    }
}