<?php

namespace Pickomenka\Utils;

class CdnUtils
{
    use SingletonTrait;

    private readonly array $mimeTypeByExtension;
    private readonly array $allowedAvatarExtensions;

    private readonly bool $cdnDisabled;
    private readonly string $cdnUrl;
    private readonly string $cdnToken;

    protected function __construct()
    {
        global $CONFIG;

        $this->cdnDisabled = $CONFIG['cdn_disabled'];
        $this->cdnUrl = $CONFIG['cdn_url'];
        $this->cdnToken = $CONFIG['cdn_token'];

        $this->mimeTypeByExtension = [
            'png' => 'image/png',
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp'
        ];
        $this->allowedAvatarExtensions = array_keys($this->mimeTypeByExtension);
    }

    /**
     * @return array
     */
    public function getMimeTypeByExtension(): array
    {
        return $this->mimeTypeByExtension;
    }

    /**
     * @return array
     */
    public function getAllowedAvatarExtensions(): array
    {
        return $this->allowedAvatarExtensions;
    }

    public function postToCdn(string $contentType, string $content): string {
        $ch = curl_init($this->cdnUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            'contentType' => $contentType,
            'content' => $content
        )));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Cdn-Token: ' . $this->cdnToken,
            'Content-Type: application/json'
        ));
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result === false) {
            http_response_code(500);
            echo json_encode([ 'error' => 'Error while contacting the CDN' ]);
            exit;
        }

        $result = json_decode($result, true);
        if ($result === null) {
            http_response_code(500);
            echo json_encode([ 'error' => 'Error while contacting the CDN' ]);
            exit;
        }

        if (!isset($result['file'])) {
            http_response_code(500);
            echo json_encode([ 'error' => 'Error while contacting the CDN' ]);
            exit;
        }

        return $result['file'];
    }

    function deleteFromCdn(string $fileName): void {
        $ch = curl_init($this->cdnUrl . $fileName);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Cdn-Token: ' . $this->cdnToken
        ));
        curl_exec($ch);
        curl_close($ch);

        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status_code !== 204 && $status_code !== 404) {
            http_response_code(500);
            echo json_encode([ 'error' => 'Error while contacting the CDN' ]);
            exit;
        }
    }

}