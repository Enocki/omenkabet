import {Injectable, isDevMode} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {lastValueFrom, Observable, take} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public API_BASE_URL: string = '';
  public CDN_URL: string = '';
  public VERSION: string = '';

  constructor(
    private readonly _httpClient: HttpClient
  ) {}

  public async loadSettings(): Promise<void> {
    await lastValueFrom(this._httpClient.get('/assets/settings.json')).then((settings: any) => {
      this.API_BASE_URL = settings.apiBaseUrl;
      this.CDN_URL = settings.cdnUrl;
      this.VERSION = settings.version;
    });
  }
}
