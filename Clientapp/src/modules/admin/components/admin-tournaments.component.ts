import {Component, OnDestroy} from '@angular/core';
import {AdminStateService} from "../services/admin-state.service";
import {Tournament} from "../../shared/models/tournament";

export type AdminTournamentsTabs = 'global' | 'matches';

@Component({
  selector: 'admin-tournaments',
  templateUrl: './admin-tournaments.component.html'
})
export class AdminTournamentsComponent implements OnDestroy {

  selectedTab: AdminTournamentsTabs = 'global';
  emptyTournament: Tournament = new Tournament({ tournamentName: 'Nouveau tournoi' });

  constructor(
    private readonly _stateService: AdminStateService
  ) { }

  ngOnDestroy(): void {
    this._stateService.tournament = undefined;
  }

  get tournament(): Tournament | undefined { return this._stateService.tournament; }
  set tournament(value: Tournament | undefined) { this._stateService.tournament = value; }

  get tournaments(): Tournament[] { return this._stateService.tournaments; }
}
