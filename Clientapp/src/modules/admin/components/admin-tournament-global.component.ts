import {Component, ViewChild} from '@angular/core';
import {AdminStateService} from "../services/admin-state.service";
import {Tournament} from "../../shared/models/tournament";
import {ApiService} from "../../shared/services/api.service";
import {SaveButtonDirective} from "../../shared/directives/save-button.directive";

@Component({
  selector: 'admin-tournament-global',
  templateUrl: './admin-tournament-global.component.html',
  host: {
    class: 'flex-column'
  }
})
export class AdminTournamentGlobalComponent {

  @ViewChild('saveButton') saveButton?: SaveButtonDirective;

  constructor(
    private readonly _stateService: AdminStateService,
    private readonly _apiService: ApiService
  ) {}

  get isNewTournament(): boolean { return this.tournament?.tournamentId === undefined; }

  get tournament(): Tournament | undefined {
    return this._stateService.tournament;
  }

  async saveTournament(): Promise<void> {
    if (this._stateService.tournament === undefined)
      return;

    if (this.isNewTournament) {
      const newTournament = await this._apiService.createTournament(this._stateService.tournament);
      this._stateService.tournaments.push(newTournament);
      this._stateService.tournament = newTournament;
    }
    else
      await this._apiService.updateTournament(this._stateService.tournament);

    this.saveButton?.animate();
  }
}
