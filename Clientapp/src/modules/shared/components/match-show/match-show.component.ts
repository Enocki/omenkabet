import {Component, Input} from '@angular/core';
import {Match} from "../../models/match";
import {Team} from "../../models/team";
import {Tournament} from "../../models/tournament";
import {AuthenticationService} from "../../services/authentication.service";
import {ApiService} from "../../services/api.service";
import {MatchBet} from "../../models/match-bet";
import {MatchService} from "../../services/match.service";

@Component({
  selector: 'match-show',
  templateUrl: './match-show.component.html',
  styleUrls: ['./match-show.component.less']
})
export class MatchShowComponent {

  private _match: Match = new Match({ isMatchDeterminedByParents: false, matchType: 1, tournamentId: 0 });

  @Input()
  set match(value: Match) {
    this._match = value;
    this._refreshTeams();
  }
  get match(): Match { return this._match; }

  private _tournament?: Tournament;
  @Input()
  set tournament(value: Tournament) {
    this._tournament = value;
    this._refreshTeams();
  }

  constructor(
    private readonly _apiService: ApiService,
    private readonly _authenticationService: AuthenticationService,
    private readonly _matchService: MatchService
  ) {}

  private _team1?: Team;
  private _team2?: Team;

  private _refreshTeams(): void {
    if (this._tournament === undefined)
      return;

    this._team1 = this._matchService.getMatchTeam1(this._match, this._tournament.matches, this._tournament.teams);
    this._team2 = this._matchService.getMatchTeam2(this._match, this._tournament.matches, this._tournament.teams);
  }

  get team1(): Team | undefined {
    return this._team1;
  }

  get team1Won(): boolean {
    return !!this._match.winner && this._match.winner === this.team1?.teamId;
  }

  get team2(): Team | undefined {
    return this._team2;
  }

  get team2Won(): boolean {
    return !!this._match.winner && this._match.winner === this.team2?.teamId;
  }

  get dateMatch(): Date | undefined {
    return this._match.startDate;
  }

  get noWinner(): boolean {
    return !this.team1Won && !this.team2Won;
  }

  get isMatchReady(): boolean {
    return this.team1 !== undefined && this.team2 !== undefined;
  }

  get canBet(): boolean {
    return this.isMatchReady && (!this._tournament?.startDate || this._tournament.startDate.getTime() > new Date().getTime()) && this.noWinner;
  }

  get haveBetOnTeam1(): boolean {
    return (this._tournament?.matchBets?.findIndex(bet => bet.matchId === this.match.matchId && bet.teamId === this.team1?.teamId && bet.userId === this._authenticationService.currentUser?.userId) ?? -1) >= 0;
  }

  async betForTeam(team: Team | undefined): Promise<void> {
    if (!this.canBet || team === undefined)
      return;

    const otherBetIndex = this._tournament?.matchBets.findIndex(bet => bet.matchId === this._match.matchId && bet.userId === this._authenticationService.currentUser?.userId) ?? -1;
    if (otherBetIndex >= 0) {
      const otherBet = this._tournament!.matchBets[otherBetIndex];
      if (otherBet.teamId === team.teamId)
        return;

      await this._apiService.deleteMatchBet(otherBet.matchBetId!);
      this._tournament!.matchBets.splice(otherBetIndex, 1);
    }

    const newBet = await this._apiService.createMatchBet(new MatchBet({
      matchBetId: undefined,
      matchId: this.match.matchId!,
      userId: this._authenticationService.currentUser!.userId,
      teamId: team.teamId!,
      loserScore: undefined
    }));

    this._tournament!.matchBets = [...this._tournament!.matchBets, newBet];
  }

  async betOnTeam1(): Promise<void> {
    await this.betForTeam(this.team1)
  }

  get haveBetOnTeam2(): boolean {
    return (this._tournament?.matchBets?.findIndex(bet => bet.matchId === this.match.matchId && bet.teamId === this.team2?.teamId && bet.userId === this._authenticationService.currentUser?.userId) ?? -1) >= 0;
  }

  async betOnTeam2(): Promise<void> {
    await this.betForTeam(this.team2)
  }
}
