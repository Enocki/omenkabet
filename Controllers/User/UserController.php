<?php

namespace Pickomenka\Controllers\User;

use Pickomenka\Controllers\AbstractController;

class UserController extends AbstractController
{
    public function get(): void
    {
        $user = $this->getLoggedUser();
        echo json_encode($user->toJson());
    }

    public function delete(): void
    {
        $this->ensureAuthentication();
        $this->updateAuthCookies(null);
        session_destroy();
    }
}
