START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE1 j2');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('2', '4', '1', '2');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('8', '10', '1', '2');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '6', '1', '2');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('7', '5', '1', '2');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('9', '3', '1', '2');

COMMIT;

START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE1 j3');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('10', '4', '1', '3');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('7', '9', '1', '3');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '5', '1', '3');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('6', '8', '1', '3');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('3', '2', '1', '3');

COMMIT;

