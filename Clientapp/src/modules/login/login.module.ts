import { NgModule } from '@angular/core';
import { LoginIndexComponent } from './components/login-index.component';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    LoginIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginIndexComponent
      }
    ])
  ]
})
export class LoginModule { }
