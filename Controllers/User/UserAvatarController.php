<?php

namespace Pickomenka\Controllers\User;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\UserDataProvider;
use Pickomenka\Utils\CdnUtils;

class UserAvatarController extends AbstractController
{
    public function put(): void
    {
        $this->ensureAuthentication();

        list($avatar, $avatarExtension) = $this->jsonBody(['avatar', 'extension']);

        if (!in_array($avatarExtension, CdnUtils::getInstance()->getAllowedAvatarExtensions()))
            $this->badRequest('Invalid extension');

        $user = $this->getLoggedUser();
        $currentAvatar = $user->getAvatar();
        if ($currentAvatar !== null)
            CdnUtils::getInstance()->deleteFromCdn($currentAvatar);

        $newAvatar = CdnUtils::getInstance()->postToCdn(CdnUtils::getInstance()->getMimeTypeByExtension()[$avatarExtension], $avatar);
        $user->setAvatar($newAvatar);

        UserDataProvider::getInstance()->updateUser($user);

        echo json_encode([ 'avatar' => $newAvatar ]);
    }
}