export interface ITournamentBet {
  tournamentBetId?: number;
  teamId: number;
  tournamentId: number;
  userId: number;
}

export class TournamentBet {
  public tournamentBetId?: number;
  public teamId: number;
  public tournamentId: number;
  public userId: number;

  constructor(data: ITournamentBet) {
    this.tournamentBetId = data['tournamentBetId'];
    this.teamId = data['teamId'];
    this.tournamentId = data['tournamentId'];
    this.userId = data['userId'];
  }

  public toJson(): any {
    return {
      'tournamentBetId': this.tournamentBetId,
      'teamId': this.teamId,
      'tournamentId': this.tournamentId,
      'userId': this.userId
    }
  }
}
