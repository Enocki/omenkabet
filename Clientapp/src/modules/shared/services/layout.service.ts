import { Injectable } from "@angular/core";
import { ColorHelper, RGB } from "../models/color-helper";

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  static defineTheme(
    primaryHex: string, primaryHueDefault: string, primaryHueLighter: string, primaryHueDarker: string,
    accentHex: string, accentHueDefault: string, accentHueLighter: string, accentHueDarker: string,
    validHex: string, validHueDefault: string, validHueLighter: string, validHueDarker: string,
    warnHex: string, warnHueDefault: string, warnHueLighter: string, warnHueDarker: string,
  ) {
    this.definePaletteColorVariables('primary', primaryHex, primaryHueDefault, primaryHueLighter, primaryHueDarker);
    this.definePaletteColorVariables('accent', accentHex, accentHueDefault, accentHueLighter, accentHueDarker);
    this.definePaletteColorVariables('valid', validHex, validHueDefault, validHueLighter, validHueDarker);
    this.definePaletteColorVariables('warn', warnHex, warnHueDefault, warnHueLighter, warnHueDarker);
  }

  static definePaletteColorVariables(paletteName: string, baseHex: string, hueDefault: string = '500', hueLighter: string = '100', hueDarker: string = '700') {
    for (const paletteColor of this.buildPaletteColors(baseHex)) {

      const paletteColorHex = ColorHelper.RGBToHex(paletteColor.rgb);
      document.documentElement.style.setProperty(`--${paletteName}-${paletteColor.label}`, paletteColorHex);

      const colorBrightness = (paletteColor.rgb.r * 299 + paletteColor.rgb.g * 587 + paletteColor.rgb.b * 114) / 1000
      const isDarkColor = colorBrightness < 149;
      document.documentElement.style.setProperty(`--${paletteName}-contrast-${paletteColor.label}`, isDarkColor ? 'white' : 'rgba(0,0,0, 0.87)');

      if (paletteColor.label == hueDefault) {
        document.documentElement.style.setProperty(`--${paletteName}-default`, paletteColorHex);
        document.documentElement.style.setProperty(`--${paletteName}-contrast-default`, isDarkColor ? 'white' : 'rgba(0,0,0, 0.87)');
      }
      if (paletteColor.label == hueLighter) {
        document.documentElement.style.setProperty(`--${paletteName}-lighter`, paletteColorHex);
        document.documentElement.style.setProperty(`--${paletteName}-contrast-lighter`, isDarkColor ? 'white' : 'rgba(0,0,0, 0.87)');
      }
      if (paletteColor.label == hueDarker) {
        document.documentElement.style.setProperty(`--${paletteName}-darker`, paletteColorHex);
        document.documentElement.style.setProperty(`--${paletteName}-contrast-darker`, isDarkColor ? 'white' : 'rgba(0,0,0, 0.87)');
      }
    }
  }

  static buildPaletteColors(baseHex: string): { rgb: RGB, label: string }[] {
    const baseColor = ColorHelper.HexToRGB(baseHex);

    return [
      { rgb: ColorHelper.Lighten(baseColor, 52), label: '50' },
      { rgb: ColorHelper.Lighten(baseColor, 37), label: '100' },
      { rgb: ColorHelper.Lighten(baseColor, 26), label: '200' },
      { rgb: ColorHelper.Lighten(baseColor, 12), label: '300' },
      { rgb: ColorHelper.Lighten(baseColor, 6), label: '400' },
      { rgb: baseColor, label: '500' },
      { rgb: ColorHelper.Darken(baseColor, 6), label: '600' },
      { rgb: ColorHelper.Darken(baseColor, 12), label: '700' },
      { rgb: ColorHelper.Darken(baseColor, 18), label: '800' },
      { rgb: ColorHelper.Darken(baseColor, 24), label: '900' },
      { rgb: ColorHelper.Saturate(ColorHelper.Lighten(baseColor, 50), 30), label: 'A100' },
      { rgb: ColorHelper.Saturate(ColorHelper.Lighten(baseColor, 30), 30), label: 'A200' },
      { rgb: ColorHelper.Saturate(ColorHelper.Lighten(baseColor, 10), 15), label: 'A400' },
      { rgb: ColorHelper.Saturate(ColorHelper.Lighten(baseColor, 5), 5), label: 'A700' }
    ];
  }
}



