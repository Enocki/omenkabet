export interface IMatchBet {
  matchBetId?: number;
  matchId: number;
  userId: number;
  teamId: number;
  loserScore?: number;
}

export class MatchBet {
  public matchBetId?: number;
  public matchId: number;
  public userId: number;
  public teamId: number;
  public loserScore?: number;

  constructor(data: IMatchBet) {
    this.matchBetId = data['matchBetId'];
    this.matchId = data['matchId'];
    this.userId = data['userId'];
    this.teamId = data['teamId'];
    this.loserScore = data['loserScore'];
  }

  public toJson(): any {
    return {
      'matchBetId': this.matchBetId,
      'matchId': this.matchId,
      'userId': this.userId,
      'teamId': this.teamId,
      'loserScore': this.loserScore
    };
  }
}
