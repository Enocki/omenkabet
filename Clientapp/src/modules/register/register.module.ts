import { NgModule } from '@angular/core';
import { RegisterIndexComponent } from './components/register-index.component';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    RegisterIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: RegisterIndexComponent
      }
    ])
  ]
})
export class RegisterModule { }
