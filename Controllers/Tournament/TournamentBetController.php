<?php

namespace Pickomenka\Controllers\Tournament;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\Database;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentBetDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Models\TournamentBetModel;
use Pickomenka\Utils\VerifyUtils;

class TournamentBetController extends AbstractController
{
    private function verifyTournamentBet(TournamentBetModel $tournamentBet): void
    {
        $tournament = TournamentDataProvider::getInstance()->readTournament($tournamentBet->getTournament());
        if ($tournament === null)
            $this->badRequest('Tournament does not exist.');

        /*if ($tournament->getWinner() !== null)
            bad_request('Tournament is already determined.');*/

        if ($tournament->getEndDate() !== null && $tournament->getEndDate()->getTimestamp() > time())
            $this->badRequest('Tournament is already finished.');

        $team = TeamDataProvider::getInstance()->readTeam($tournamentBet->getTeam());
        if ($team === null)
            $this->badRequest('Team does not exist.');

        $rs = Database::getInstance()->querySingle(
            'SELECT COUNT(matchid) FROM matchs WHERE tournamentid = ? AND (team1 = ? OR team2 = ?)',
            [$tournamentBet->getTournamentBetId(), $tournamentBet->getTeam(), $tournamentBet->getTeam()]
        );
        if (count($rs) == 0 || $rs == null)
            $this->badRequest('Team not found in this tournament.');

    }
    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournamentBet = TournamentBetDataProvider::getInstance()->readTournamentBet($id);
        if ($tournamentBet === null)
            $this->notFound();

        echo json_encode($tournamentBet->toJson());
    }

    public function post(): void
    {
        $this->ensureAuthentication();

        $tournamentBet = TournamentBetModel::fromJson($this->jsonBody(), $this->getLoggedUserId());

        $this->verifyTournamentbet($tournamentBet);
        TournamentBetDataProvider::getInstance()->createTournamentBet($tournamentBet);

        echo json_encode($tournamentBet->toJson());
    }

    public function put(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournamentBet = TournamentBetModel::fromJson($this->jsonBody(), $this->getLoggedUserId());
        if ($tournamentBet->getTournamentBetId() !== $id)
            $this->badRequest('Id mismatch');

        $existingTournamentBet = TournamentBetDataProvider::getInstance()->readTournamentBet($id);
        if ($existingTournamentBet === null)
            $this->notFound();

        if($existingTournamentBet->getUser() !== $this->getLoggedUserId())
            $this->forbidden();

        $this->verifyTournamentbet($tournamentBet);
        TournamentBetDataProvider::getInstance()->updateTournamentBet($tournamentBet);

        echo json_encode($tournamentBet->toJson());
    }

    public function delete(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournamentBet = TournamentBetDataProvider::getInstance()->readTournamentBet($id);
        if($tournamentBet === null)
            $this->notFound();

        if($tournamentBet->getUser() !== $this->getLoggedUserId())
            $this->forbidden();

        TournamentBetDataProvider::getInstance()->deleteTournamentBet($id);

        echo json_encode($tournamentBet->toJson());
    }
}
