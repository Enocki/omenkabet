import {NgModule} from '@angular/core';
import {AdminIndexComponent} from './components/admin-index.component';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {AdminTournamentMatchesComponent} from './components/admin-tournament-matches.component';
import {AdminTournamentsComponent} from './components/admin-tournaments.component';
import {AdminTournamentGlobalComponent} from './components/admin-tournament-global.component';
import {AdminTeamsComponent} from './components/admin-teams.component';
import {AdminUsersComponent} from './components/admin-users.component';


@NgModule({
  declarations: [
    AdminIndexComponent,
    AdminTournamentMatchesComponent,
    AdminTournamentsComponent,
    AdminTournamentGlobalComponent,
    AdminTeamsComponent,
    AdminUsersComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdminIndexComponent
      }
    ])
  ]
})
export class AdminModule { }
