<?php

namespace Pickomenka\Controllers\Tournament;

use DateTime;
use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Utils\MatchUtils;
use Pickomenka\Utils\VerifyUtils;

class TournamentStatsController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($tournament === null)
            $this->notFound();

        $teams = TeamDataProvider::getInstance()->readTeamsByTournament($id);
        $matches = MatchDataProvider::getInstance()->readMatchesWithMatchBets($id);
        $stats = [];

        foreach ($matches as $match) {
            if ($tournament->getStartDate() === null || $tournament->getStartDate()->getTimestamp() > (new DateTime())->getTimestamp())
                continue;

            $team1 = MatchUtils::getMatchTeam1($match, $matches, $teams);
            $team2 = MatchUtils::getMatchTeam2($match, $matches, $teams);

            $votesFor1 = 0;
            $votesFor2 = 0;

            if ($team1 !== null || $team2 !== null)
                foreach ($match->getMatchBet() as $bet) {
                    if ($bet->getTeamId() === $team1?->getTeamId())
                        $votesFor1++;
                    else if ($bet->getTeamId() === $team2?->getTeamId())
                        $votesFor2++;
                }

            if ($votesFor1 + $votesFor2 !== count($match->getMatchBet()))
                $this->internalServerError("Invalid match bets.");

            $total = $votesFor1 + $votesFor2;
            $stats[] = [
                'matchId' => $match->getMatchId(),
                'team1' => $team1?->getTeamId(),
                'teamName1' => $team1?->getTeamName(),
                'users1' => $votesFor1,
                'team2' => $team2?->getTeamId(),
                'teamName2' => $team2?->getTeamName(),
                'users2' => $votesFor2,
                'statTeam1' => $total === 0 ? null : round($votesFor1 / $total * 100, 2),
                'statTeam2' => $total === 0 ? null : round($votesFor2 / $total * 100, 2),
                'winner' => $match->getWinner(),
                'total' => $total
            ];
        }

        echo json_encode($stats);
    }
}