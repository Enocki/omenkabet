import {Component, ViewChild} from '@angular/core';
import {AuthenticationService} from "../../shared/services/authentication.service";
import {Router} from "@angular/router";
import {ApiError} from "../../shared/models/api-error";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'register-index',
  templateUrl: './register-index.component.html'
})
export class RegisterIndexComponent {
  username: string = '';
  email: string = '';
  password: string = '';

  @ViewChild('form') form!: NgForm;

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _router: Router
  ) { }

  async formSubmitted(): Promise<void> {
    this.username = this.username.trim(); // remove white spaces
    this.email = this.email.trim();

    if (!(this.form.valid ?? false))
      return;

    try {
      await this._authenticationService.register(this.email, this.username, this.password);
      await this._router.navigate(['/profile']);
    }
    catch (e: any) {
      if (e instanceof ApiError) {
        alert(e.details['error_message']);
      }
      else
        throw e;
    }
  }

}
