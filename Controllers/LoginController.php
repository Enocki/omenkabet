<?php

namespace Pickomenka\Controllers;

use Pickomenka\Database\UserDataProvider;

class LoginController extends AbstractController
{
    public function post(): void
    {
        list($login, $password) = $this->jsonBody(['login', 'password']);

        $user = UserDataProvider::getInstance()->readUserByLogin($login);

        if ($user === null || !password_verify($password, $user->getPassword()))
            $this->unauthorized('Basic');

        $_SESSION['userid'] = $user->getUserId();
        $this->updateAuthCookies($user->getAuthToken());

        echo json_encode($user->toJson());
    }
}
