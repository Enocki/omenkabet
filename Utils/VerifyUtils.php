<?php

namespace Pickomenka\Utils;

use JetBrains\PhpStorm\NoReturn;

class VerifyUtils
{
    #[NoReturn] private static function verificationFail(string $message): void
    {
        http_response_code(400);
        echo json_encode(['status_code' => 400, 'error_message' => $message]);
        exit();
    }

    /**
     * Check and process a give text
     * @param mixed $text The text to process
     * @param int $minLength The inclusive minimum length of the text
     * @param int $maxLength The inclusive maximum length of the text
     * @param bool $allowNull If true, null is allowed
     * @param bool $allowEmpty If true, empty string is allowed
     * @param bool $convertToNullIfEmpty If true, empty string will be converted to null
     * @param bool $trim If true, the text will be trimmed
     * @return string|null The processed text
     */
    public static function verifyString(mixed $text, int $minLength = -1, int $maxLength = -1, bool $allowNull = false, bool $allowEmpty = false, bool $convertToNullIfEmpty = true, bool $trim = true): string|null
    {
        if ($convertToNullIfEmpty && $text === '')
            $text = null;

        if ($text === null && !$allowNull)
            self::verificationFail('Null text');

        if ($text === null)
            return null;

        if (gettype($text) !== 'string')
            self::verificationFail('Invalid text');

        if ($trim)
            $text = trim($text);

        if ($text === '' && !$allowEmpty)
            self::verificationFail('Empty text');

        if ($minLength !== -1 && strlen($text) < $minLength)
            self::verificationFail('Text too short');

        if ($maxLength !== -1 && strlen($text) > $maxLength)
            self::verificationFail('Text too long');

        return $text;
    }


    /**
     * Check and process a given number
     * @param mixed $number The number to process
     * @param bool $mustBePositive If true, the number must be positive
     * @param bool $allowZero If true, zero is allowed
     * @param bool $allowNull If true, null is allowed
     * @param ?int $minValue The minimum value the number should be (null to not check)
     * @param ?int $maxValue The maximum value the number should be (null to not check)
     * @return ?int The processed number
     */
    public static function verifyNumber(mixed $number, bool $mustBePositive = true, bool $allowZero = false, bool $allowNull = false, ?int $minValue = null, ?int $maxValue = null): ?int
    {
        if (gettype($number) === 'string') {
            if (preg_match('/\d+/', $number) === 1)
                $number = intval($number);
            else if (strlen($number) !== 0)
                self::verificationFail('Number cannot be empty: ' . $number);
            else
                $number = null;
        }

        if ($number === null && !$allowNull)
            self::verificationFail('Number cannot be null: ' . $number);

        if ($number !== null) {
            if (gettype($number) !== 'integer')
                self::verificationFail('Number is not an integer: ' . $number);

            if ($number < 0 && $mustBePositive)
                self::verificationFail('Number must be positive: ' . $number);

            if ($number === 0 && !$allowZero)
                self::verificationFail('Number cannot be zero: ' . $number);

            if ($minValue !== null && $number < $minValue)
                self::verificationFail('Number is too small: ' . $number);

            if ($maxValue !== null && $number > $maxValue)
                self::verificationFail('Number is too big: ' . $number);
        }

        return $number;
    }

}
