<?php

namespace Pickomenka\Database;

use DateTime;
use Exception;
use Pickomenka\Exceptions\InternalErrorException;
use Pickomenka\Models\TournamentModel;
use Pickomenka\Utils\SingletonTrait;

class TournamentDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    public function createTournament(TournamentModel $tournament): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO tournaments (tournamentname, startdate, enddate) VALUES (?, ?, ?)',
            [$tournament->getTournamentName(), $tournament->getStartDate()?->getTimestamp(), $tournament->getEndDate()?->getTimestamp()]
        );

        $tournament->setTournamentId($result->lastInsertId);
    }

    public function readTournament(int $tournamentId): TournamentModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT tournamentid, avatar, tournamentname, startdate, enddate FROM tournaments WHERE tournamentid = ?',
            [$tournamentId]
        );

        if ($rs === null)
            return null;

        return new TournamentModel(
            $rs['tournamentid'],
            $rs['tournamentname'],
            $rs['startdate'] === null ? null : new DateTime('@' . $rs['startdate']),
            $rs['enddate'] === null ? null : new DateTime('@' . $rs['enddate']),
            $rs['avatar']
        );
    }

    /**
     * @throws InternalErrorException
     * @throws Exception
     */
    public function readNextTournament(): TournamentModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT tournamentid, avatar, tournamentname, startdate, enddate FROM tournaments WHERE startdate IS NOT NULL AND (enddate IS NULL OR enddate > ?) ORDER BY startdate LIMIT 1',
            [(new DateTime())->getTimestamp()]
        );

        if ($rs === null)
            return null;

        return new TournamentModel(
            $rs['tournamentid'],
            $rs['tournamentname'],
            $rs['startdate'] === null ? null : new DateTime('@' . $rs['startdate']),
            $rs['enddate'] === null ? null : new DateTime('@' . $rs['enddate']),
            $rs['avatar']
        );
    }

    public function readPreviousTournamentId(TournamentModel $tournament): int | null
    {
        $startDate = $tournament->getStartDate()?->getTimestamp();
        if ($startDate === null)
            return null;

        $row = Database::getInstance()->querySingle(
            'SELECT tournamentid FROM tournaments WHERE startdate IS NOT NULL AND tournamentid != ? AND startdate < ? ORDER BY startdate DESC LIMIT 1;',
            [$tournament->getTournamentId(), $startDate]
        );

        if ($row === null)
            return null;

        return $row['tournamentid'];
    }

    public function readNextTournamentId(TournamentModel $tournament): int | null
    {
        $startDate = $tournament->getStartDate()?->getTimestamp();
        if ($startDate === null)
            return null;

        $row = Database::getInstance()->querySingle(
            "SELECT * FROM tournaments WHERE startdate IS NOT NULL AND tournamentid != ? AND startdate > ? ORDER BY startdate LIMIT 1;",
            [$tournament->getTournamentId(), $startDate]
        );

        if ($row === null)
            return null;

        return $row['tournamentid'];
    }

    public function readTournaments(): array
    {
        $rs = Database::getInstance()->queryAll('SELECT tournamentid, avatar, tournamentname, startdate, enddate FROM tournaments', []);

        return array_map(function (array $row) {
            return new TournamentModel(
                $row['tournamentid'],
                $row['tournamentname'],
                $row['startdate'] === null ? null : new DateTime('@' . $row['startdate']),
                $row['enddate'] === null ? null : new DateTime('@' . $row['enddate']),
                $row['avatar']
            );
        }, $rs);
    }

    public function updateTournament(TournamentModel $tournament, bool $updateAvatar): void
    {
        $query = 'UPDATE tournaments SET tournamentname = ?, startdate = ?, enddate = ?';
        $params = [$tournament->getTournamentName(), $tournament->getStartDate()?->getTimestamp(), $tournament->getEndDate()?->getTimestamp()];

        if ($updateAvatar) {
            $query .= ', avatar = ?';
            $params[] = $tournament->getAvatar();
        }

        $query .= ' WHERE tournamentid = ?';
        $params[] = $tournament->getTournamentId();

        Database::getInstance()->update($query, $params);
    }

    public function deleteTournament(int $tournamentId): void
    {
        Database::getInstance()->update(
            'DELETE FROM tournaments WHERE tournamentid = ?',
            [$tournamentId]
        );
    }

}