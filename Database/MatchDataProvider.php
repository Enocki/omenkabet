<?php

namespace Pickomenka\Database;

use DateTime;
use Pickomenka\Models\MatchBetModel;
use Pickomenka\Models\MatchModel;
use Pickomenka\Utils\SingletonTrait;

class MatchDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    public function createMatch(MatchModel $match): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO matchs (parentmatch1, parentmatch2, team1, team2, winner, startdate, matchtype, 
                        loserscore, tournamentid)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [$match->getParentMatchId1(), $match->getParentMatchId2(), $match->getTeamId1(), $match->getTeamId2(),
                $match->getWinner(), $match->getStartDate()?->getTimestamp(), $match->getMatchType(), $match->getLoserScore(),
                $match->getTournamentId()]
        );

        $match->setMatchId($result->lastInsertId);
    }

    public function readMatch(int $matchId) : MatchModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT matchid, parentmatch1, parentmatch2, team1, team2, winner, startdate, matchtype, loserscore, 
                    tournamentid FROM matchs WHERE matchid = ?',
            [$matchId]
        );

        if ($rs === null)
            return null;

        return new MatchModel(
            $rs['matchid'],
            $rs['parentmatch1'],
            $rs['parentmatch2'],
            $rs['team1'],
            $rs['team2'],
            $rs['winner'],
            $rs['startdate'] === null ? null : DateTime::createFromFormat('U', $rs['startdate']),
            $rs['matchtype'],
            $rs['loserscore'],
            $rs['tournamentid']
        );
    }

    /**
     * @param int $tournamentId
     * @return MatchModel[]
     */
    public function readMatchesByTournament(int $tournamentId): array
    {
        $rs = Database::getInstance()->queryAll(
            'SELECT matchid, parentmatch1, parentmatch2, team1, team2, winner, startdate, matchtype, loserscore, 
                    tournamentid FROM matchs WHERE tournamentid = ?',
            [$tournamentId]
        );

        $matches = [];
        foreach ($rs as $row) {
            $matches[] = new MatchModel(
                $row['matchid'],
                $row['parentmatch1'],
                $row['parentmatch2'],
                $row['team1'],
                $row['team2'],
                $row['winner'],
                $row['startdate'] === null ? null : DateTime::createFromFormat('U', $row['startdate']),
                $row['matchtype'],
                $row['loserscore'],
                $row['tournamentid']
            );
        }

        return $matches;
    }


    /**
     * @param int $tournamentId
     * @return MatchModel[]
     */
    public function readMatchesWithMatchBets(int $tournamentId): array {

        $rs = Database::getInstance()->queryAll(
            '
                SELECT distinct matchs.*, matchbets.*
                FROM matchs
                LEFT JOIN matchbets ON matchs.matchid = matchbets.match
                WHERE matchs.tournamentid = ?
                ORDER BY matchs.startdate;
            ',
            [$tournamentId]
        );

        $matches = [];
        foreach ($rs as $row) {
            if (!isset($matches[$row['matchid']])) {
                $matches[$row['matchid']] = new MatchModel(
                    $row[0],
                    $row[1],
                    $row[2],
                    $row[3],
                    $row[4],
                    $row[5],
                    $row[6] === null ? null : DateTime::createFromFormat('U', $row[6]),
                    $row[7],
                    $row[8],
                    $row[9]
                );
            }

            if ($row['matchbetid'] !== null) {
                $matches[$row['matchid']]->addMatchBet(
                    new MatchBetModel(
                        $row[10],
                        $row[11],
                        $row[12],
                        $row[13],
                        $row[14]
                    )
                );
            }
        }

        return $matches;
    }

    public function updateMatch(MatchModel $match): void {
        Database::getInstance()->update(
            'UPDATE matchs 
                    SET parentmatch1 = ?, parentmatch2 = ?, team1 = ?, team2 = ?,
                        winner = ?, startdate = ?, matchtype = ?, loserscore = ?,
                        tournamentid = ? 
                    WHERE matchid = ?',
            [$match->getParentMatchId1(), $match->getParentMatchId2(), $match->getTeamId1(), $match->getTeamId2(),
                $match->getWinner(), $match->getStartDate()?->getTimestamp(), $match->getMatchType(), $match->getLoserScore(),
                $match->getTournamentId(),
                $match->getMatchId()]
        );
    }

    public function deleteMatch(int $matchId): void
    {
        Database::getInstance()->update(
            'DELETE FROM matchs WHERE matchid = ?',
            [$matchId]
        );
    }

}
