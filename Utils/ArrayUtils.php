<?php

namespace Pickomenka\Utils;

class ArrayUtils
{

    /**
     * Check the fields
     *
     * @param array $decoded
     * @param array $mandatoryFields
     * @param array $optionalFields
     * @return array
     */
    public static function checkArrayFields(array $decoded, array $mandatoryFields = [], array $optionalFields = []):array {
        $missingFields = [];
        $sortedFields = [];
        foreach ($mandatoryFields as $field) {
            if (!array_key_exists($field, $decoded))
                $missingFields[] = $field;
            else {
                $sortedFields[] = $decoded[$field];
                unset($decoded[$field]);
            }
        }

        if (count($missingFields) > 0) {
            http_response_code(400);
            echo json_encode(['error' => 'Missing field in JSON: ' . implode(', ', $missingFields)]);
            exit();
        }

        foreach ($optionalFields as $field) {
            if (!array_key_exists($field, $decoded))
                $sortedFields[] = null;
            else {
                $sortedFields[] = $decoded[$field];
                unset($decoded[$field]);
            }
        }

        return array_merge($sortedFields, $decoded);
    }
}