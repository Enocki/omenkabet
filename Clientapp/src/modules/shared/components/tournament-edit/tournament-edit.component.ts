import {Component, Input} from '@angular/core';
import {Tournament} from "../../models/tournament";
import {PictureUtils} from "../../models/picture-utils";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'tournament-edit',
  templateUrl: './tournament-edit.component.html',
  host: {
    class: 'flex-column gap-3'
  }
})
export class TournamentEditComponent {
  private _tournament?: Tournament;
  @Input()
  set tournament(value: Tournament) {
    this._tournament = value;
  }

  constructor(
    private readonly _apiService: ApiService
  ) {}

  get isNewTournament(): boolean { return this._tournament?.tournamentId === undefined; }

  get tournamentName(): string | undefined {
    return this._tournament?.tournamentName;
  }

  set tournamentName(value: string | undefined) {
    if (this._tournament === undefined)
      return;

    this._tournament.tournamentName = value ?? '';
  }

  get avatar(): string | undefined {
    return this._tournament?.avatar;
  }

  async handleAvatarChange(event: Event) {
    PictureUtils.preparePictureAfterEvent(event, async (base64, extension) => {
      if (!this._tournament)
        throw new Error('Tournament is undefined');

      this._tournament.avatar =
        await this._apiService.updateTournamentAvatar(
          this._tournament.tournamentId!, base64, extension
        );
    });
  }

  get startDate(): string {
    let startDate = this._tournament?.startDate;
    if (startDate === undefined)
      return '';

    startDate = new Date(startDate);
    startDate.setTime(startDate.getTime() - startDate.getTimezoneOffset() * 60 * 1000);

    return startDate.toISOString().slice(0, 16);
  }

  set startDate(value: string) {
    if (this._tournament === undefined)
      return;

    this._tournament.startDate = new Date(value);
  }

  get endDate(): string {
    let endDate = this._tournament?.endDate;
    if (endDate === undefined)
      return '';

    endDate = new Date(endDate);
    endDate.setTime(endDate.getTime() - endDate.getTimezoneOffset() * 60 * 1000);

    return endDate.toISOString().slice(0, 16);
  }

  set endDate(value: string) {
    if (this._tournament === undefined)
      return;

    this._tournament.endDate = new Date(value);
  }
}
