import { Component } from '@angular/core';
import {AuthenticationService} from "../../shared/services/authentication.service";
import {ApiError} from "../../shared/models/api-error";
import {Router} from "@angular/router";

@Component({
  selector: 'login-index',
  templateUrl: './login-index.component.html'
})
export class LoginIndexComponent {
  login: string = '';
  password: string = '';

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _router: Router
  ) { }

  async formSubmitted(): Promise<void> {
    this.login = this.login.trim(); // remove white spaces

    // If form is empty
    if (this.login.length === 0 || this.password.length === 0)
      return;

    try {
      await this._authenticationService.login(this.login, this.password);
      await this._router.navigate(['/']);
    }
    catch (e: any) {
      if (e instanceof ApiError) {
        alert(e.details['error_message']);
      }
      else
        throw e;
    }
  }
}
