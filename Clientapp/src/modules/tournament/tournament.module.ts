import { NgModule } from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import { TournamentIndexComponent } from './components/tournament-index.component';
import { TournamentShowComponent } from './components/tournament-show.component';
import { TournamentMatchesComponent } from './components/tournament-matches/tournament-matches.component';
import { TournamentRankingComponent } from './components/tournament-ranking/tournament-ranking.component';
import { TournamentStatsComponent } from './components/tournament-stats/tournament-stats.component';


@NgModule({
  declarations: [
    TournamentIndexComponent,
    TournamentShowComponent,
    TournamentMatchesComponent,
    TournamentRankingComponent,
    TournamentStatsComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: ':id',
        component: TournamentShowComponent
      },
      {
        path: '',
        component: TournamentIndexComponent
      }
    ])
  ],
  exports: []
})
export class TournamentModule { }
