export interface ITeam {
  teamId?: number;
  teamName: string;
  avatar?: string;
}

export class Team {
  public teamId?: number;
  public teamName: string;
  public avatar?: string;

  constructor(data: ITeam) {
    this.teamId = data['teamId'];
    this.teamName = data['teamName'];
    this.avatar = data['avatar'];
  }

  public toJson(): any {
    return {
      teamId: this.teamId,
      teamName: this.teamName,
      avatar: this.avatar
    }
  }
}
