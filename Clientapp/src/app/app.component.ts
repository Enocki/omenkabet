import {Component, OnInit} from '@angular/core';
import {LayoutService} from "../modules/shared/services/layout.service";
import {AuthenticationService} from "../modules/shared/services/authentication.service";
import {Router} from "@angular/router";
import {SettingsService} from "../modules/shared/services/settings.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  host: {
    class: 'w-100 mh-100 flex-column'
  }
})
export class AppComponent implements OnInit{
  private _authenticationService: AuthenticationService;

  constructor(
    authenticationService: AuthenticationService,
    private readonly _router: Router,
    private settingsService: SettingsService
  ) {
    this._authenticationService = authenticationService;
  }

  async ngOnInit(): Promise<void> {
    LayoutService.defineTheme(
      '#1E1E1E', '500', '500', '800',
      '#F6F6F6', '500', '100', '800',
      '#c2197c', '500', '100', '800',
      '#fc1b07', '500', '100', '800',
    );

    await this._authenticationService.refreshCurrentUser();
  }

  get hideHeader(): boolean {
    return this._router.routerState.snapshot.root.firstChild?.data['hideHeader'] ?? false;
  }

  get hideFooter(): boolean {
    return this._router.routerState.snapshot.root.firstChild?.data['hideFooter'] ?? false;
  }

  get isAdmin(): boolean {
    return this._authenticationService.isAdmin;
  }

  get isAuthenticated(): boolean {
    return this._authenticationService.isAuthenticated;
  }

  get version(): string {
    return this.settingsService.VERSION;
  }

}
