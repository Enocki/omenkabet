import { Directive } from '@angular/core';

@Directive({
  selector: 'button[pickomenka-button]',
  host: {
    class: 'border-rounded bg-color-primary text-primary-contrast',
    style: 'border: 0'
  }
})
export class PickomenkaButtonDirective {

  constructor() { }

}
