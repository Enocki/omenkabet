<?php

namespace Pickomenka\Models;

use Pickomenka\Utils\ArrayUtils;
use Pickomenka\Utils\VerifyUtils;

class TeamModel
{
    private int $teamId;
    private string $teamName;
    private ?string $avatar;

    public function __construct(int $teamId, string $teamName, ?string $avatar)
    {
        $this->teamId = $teamId;
        $this->teamName = $teamName;
        $this->avatar = $avatar;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return string
     */
    public function getTeamName(): string
    {
        return $this->teamName;
    }

    /**
     * @param string $teamName
     */
    public function setTeamName(string $teamName): void
    {
        $this->teamName = $teamName;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(?string $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return array
     */
    public function toJson(): array
    {
        return [
            'teamId' => $this->getTeamId(),
            'teamName' => $this->getTeamName(),
            'avatar' => $this->getAvatar(),
        ];
    }

    /**
     * @param array $json
     * @return TeamModel
     */
    public static function fromJson(array $json): TeamModel
    {
        list($teamName, $teamId) = ArrayUtils::checkArrayFields($json, ['teamName'], ['teamId']);

        $teamName = VerifyUtils::verifyString($teamName, 0, 45);
        $teamId = VerifyUtils::verifyNumber($teamId, allowNull: true);

        return new TeamModel($teamId ?? 0, $teamName, null);
    }
}