<?php

namespace Pickomenka\Controllers\Tournament;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\MatchBetDataProvider;
use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Models\MatchBetModel;
use Pickomenka\Models\MatchModel;
use Pickomenka\Models\TeamModel;
use Pickomenka\Models\TournamentModel;

class TournamentsController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $withDetails = isset($this->queryVars['withDetails']) && $this->queryVars['withDetails'] === 'true';

        $dataTournaments = TournamentDataProvider::getInstance()->readTournaments();
        $viewTournaments = array_map(function (TournamentModel $tournament) {
            return $tournament->toJson();
        }, $dataTournaments);

        if ($withDetails) {
            $this->ensureAdmin();

            foreach ($viewTournaments as &$viewTournament) {
                $viewTournament['matches'] = array_map(function (MatchModel $match) {
                    return $match->toJson();
                }, MatchDataProvider::getInstance()->readMatchesByTournament($viewTournament['tournamentId']));

                $viewTournament['teams'] = array_map(function (TeamModel $team) {
                    return $team->toJson();
                }, TeamDataProvider::getInstance()->readTeamsByTournament($viewTournament['tournamentId']));

                $viewTournament['matchBets'] = array_map(function (MatchBetModel $matchBet) {
                    return $matchBet->toJson();
                }, MatchBetDataProvider::getInstance()->readMatchbetsByTournament($viewTournament['tournamentId']));
            }
        }

        echo json_encode($viewTournaments);
    }
}
