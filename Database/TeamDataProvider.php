<?php

namespace Pickomenka\Database;

use Pickomenka\Models\TeamModel;
use Pickomenka\Utils\SingletonTrait;

class TeamDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    public function createTeam(TeamModel $team): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO teams (teamname, avatar) VALUES (?, ?)',
            [$team->getTeamName(), $team->getAvatar()]
        );

        $team->setTeamId($result->lastInsertId);
    }

    public function readTeam(int $teamId): TeamModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT teamid, teamname, avatar FROM teams WHERE teamid = ?',
            [$teamId]
        );

        if ($rs === null)
            return null;

        return new TeamModel(
            $rs['teamid'],
            $rs['teamname'],
            $rs['avatar']
        );
    }

    /**
     * @return TeamModel[]
     */
    public function readTeams(): array
    {
        $rs = Database::getInstance()->queryAll('SELECT teamid, teamname, avatar FROM teams', []);

        return array_map(function ($row) {
            return new TeamModel(
                $row['teamid'],
                $row['teamname'],
                $row['avatar']
            );
        }, $rs);
    }

    /**
     * @param int $tournamentId
     * @return TeamModel[]
     */
    public function readTeamsByTournament(int $tournamentId): array
    {
        $rs = Database::getInstance()->queryAll(
            'SELECT teamid, teamname, avatar FROM teams WHERE teamid IN (SELECT team1 FROM matchs WHERE tournamentid = ? UNION SELECT team2 FROM matchs WHERE tournamentid = ?)',
            [$tournamentId, $tournamentId]
        );

        $teams = [];
        foreach ($rs as $row) {
            $teams[] = new TeamModel(
                $row['teamid'],
                $row['teamname'],
                $row['avatar']
            );
        }

        return $teams;
    }

    public function updateTeam(TeamModel $team, bool $updateAvatar = false): void
    {
        $query = 'UPDATE teams SET teamname = ? ';
        $params = [$team->getTeamName()];

        if ($updateAvatar) {
            $query .= ', avatar = ? ';
            $params[] = $team->getAvatar();
        }

        $query .= 'WHERE teamid = ?';
        $params[] = $team->getTeamId();

        Database::getInstance()->update($query, $params);
    }

    public function deleteTeam(int $teamId): void
    {
        Database::getInstance()->update(
            'DELETE FROM teams WHERE teamid = ?',
            [$teamId]
        );
    }
}
