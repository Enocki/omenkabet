import {CanActivateFn, Router, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
import {inject} from "@angular/core";
import {AuthenticationService} from "../services/authentication.service";

export const authenticationGuard: CanActivateFn = (route, state): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree => {
  const authenticationService = inject(AuthenticationService);
  const router = inject(Router);

  return authenticationService.getCurrentUserPromise().then(async user => {
    if (user !== undefined)
      return true;

    await router.navigate(['login'])
    return false;
  });
};
