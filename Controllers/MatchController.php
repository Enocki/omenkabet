<?php

namespace Pickomenka\Controllers;

use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Models\MatchModel;
use Pickomenka\Utils\VerifyUtils;

class MatchController extends AbstractController
{
    private function verifyMatch(MatchModel $match): void
    {
        $tournament = TournamentDataProvider::getInstance()->readTournament($match->getTournamentId());
        if ($tournament === null)
            $this->badRequest('Tournament does not exist.');

        if ($match->isMatchDeterminedByParents()) {
            $parentMatch1 = MatchDataProvider::getInstance()->readMatch($match->getParentMatchId1());
            if ($parentMatch1 === null)
                $this->badRequest('Parent match 1 does not exist.');

            $parentMatch2 = MatchDataProvider::getInstance()->readMatch($match->getParentMatchId2());
            if ($parentMatch2 === null)
                $this->badRequest('Parent match 2 does not exist.');
        }
        else {
            $team1 = TeamDataProvider::getInstance()->readTeam($match->getTeamId1());
            if ($team1 === null)
                $this->badRequest('Team 1 does not exist.');

            $team2 = TeamDataProvider::getInstance()->readTeam($match->getTeamId2());
            if ($team2 === null)
                $this->badRequest('Team 2 does not exist.');
        }

        if (!$match->isMatchDeterminedByParents() && $match->getTeamId1() === $match->getTeamId2())
            $this->badRequest('Team 1 and team 2 must be different.');

        if ($match->isMatchDeterminedByParents() && $match->getParentMatchId1() === $match->getParentMatchId2())
            $this->badRequest('Parent match 1 and parent match 2 must be different.');

        if ($match->getWinner() !== null) {
            $winner = TeamDataProvider::getInstance()->readTeam($match->getWinner());
            if ($winner === null)
                $this->badRequest('Winner does not exist.');
        }
    }

    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $match = MatchDataProvider::getInstance()->readMatch($id);
        if ($match === null)
            $this->notFound();

        echo json_encode($match->toJson());
    }

    public function post(): void
    {
        $this->ensureAdmin();

        $match = MatchModel::fromJson($this->jsonBody());
        $this->verifyMatch($match);

        MatchDataProvider::getInstance()->createMatch($match);

        echo json_encode($match->toJson());
    }

    public function put(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $match = MatchModel::fromJson($this->jsonBody());
        if ($match->getMatchId() !== $id)
            $this->badRequest('Id mismatch.');

        $existingMatch = MatchDataProvider::getInstance()->readMatch($id);
        if ($existingMatch === null)
            $this->notFound();

        $this->verifyMatch($match);
        MatchDataProvider::getInstance()->updateMatch($match);

        echo json_encode($match->toJson());
    }

    public function delete(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $existingMatch = MatchDataProvider::getInstance()->readMatch($id);
        if ($existingMatch === null)
            $this->notFound();

        MatchDataProvider::getInstance()->deleteMatch($id);
    }
}