import {Component, ElementRef, Input} from "@angular/core";

@Component({
  selector: 'shared-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.less']
})
export class ProgressBarComponent {
  @Input()
  set progressValue(value: number) {
    this.elementRef.nativeElement.style.setProperty('--progress-value', (value + '%'));
  }

  constructor(private elementRef: ElementRef<HTMLElement>) {
  }


}
