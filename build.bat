REM Clear the build directory
rmdir /s /q build
mkdir build

REM Build and copy the Angular application
cd clientapp
call ng build
cd ..
Xcopy /E /I clientapp\dist\pickomenka build

REM Copy the api
mkdir build\api
robocopy .\ .\build\api\ /e /xd clientapp .git\ .idea\ Clientapp\ Config\ vendor\ build\ /xf .gitignore build.bat README.md .cpanel.yml .htaccess

REM Copy the .htaccess files
copy Config\.api-htaccess build\api\.htaccess
copy Config\.root-htaccess build\.htaccess

REM Copy the correct config and settings
mkdir build\api\Config
copy Config\production_config.php build\api\Config\config.php
copy build\assets\settings_production.json build\assets\settings.json
del build\assets\settings_production.json

REM Composer install
cd build\api
composer install --no-dev --optimize-autoloader

echo:
echo:
echo:
echo Pickomenka has been built !
echo: