import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: 'button[saveButton]',
  exportAs: 'saveButton'
})
export class SaveButtonDirective {

  constructor(
    private readonly _elementRef: ElementRef<HTMLElement>
  ) { }

  animate(): void {
    this._elementRef.nativeElement.animate([
      { backgroundColor: 'rgb(0,212,14)' },
      { backgroundColor: 'inherit' }
    ],
    {
      duration: 1000,
      iterations: 1
    })
  }
}
