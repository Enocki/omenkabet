<?php

$CONFIG = [
    'production_mode' => false,

    'uri_prefix' => '/pickomenka/api/',
    'allowed_origin' => 'http://localhost:4200',

    'cdn_disabled' => false,
    'cdn_token' => 'abcde',
    'cdn_url' => 'http://localhost/cdn/',

    'database_host' => '127.0.0.1',
    'database_dbname' => 'omenkabet',
    'database_user' => 'root',
    'database_password' => ''
];
