<?php

namespace Pickomenka\Controllers;

use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Models\TeamModel;

class TeamsController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $dataTeams = TeamDataProvider::getInstance()->readTeams();
        $viewTeams = array_map(function (TeamModel $tournament) {
            return $tournament->toJson();
        }, $dataTeams);

        echo json_encode($viewTeams);
    }
}
