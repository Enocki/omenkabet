import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../shared/services/authentication.service";
import {Tournament} from "../../shared/models/tournament";
import {ApiService} from "../../shared/services/api.service";
import {ApiError} from "../../shared/models/api-error";

@Component({
  selector: 'welcome-index',
  templateUrl: './welcome-index.component.html',
  styleUrls: ['./welcome-index.component.less']
})
export class WelcomeIndexComponent implements OnInit {

  _authenticationService: AuthenticationService;
  nextTournament?: Tournament;

  constructor(
    authenticationService: AuthenticationService,
    private readonly _apiService: ApiService
  ) {
    this._authenticationService = authenticationService;
  }

  async ngOnInit(): Promise<void> {
    await this._authenticationService.getCurrentUserPromise();

    if (this.isAuthenticated)
      try {
        this.nextTournament = await this._apiService.readNextTournament();
      } catch (e: any) {
        if (!(e instanceof ApiError) || e.statusCode !== 404)
          throw e;
      }
  }

  get isAuthenticated(): boolean {
    return this._authenticationService.isAuthenticated;
  }

  get avatarUser(): string | undefined {
    return this._authenticationService.currentUser?.avatar;
  }
}
