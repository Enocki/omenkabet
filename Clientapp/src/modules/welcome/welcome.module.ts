import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import { WelcomeIndexComponent } from './component/welcome-index.component';
import {SharedModule} from "../shared/shared.module";



@NgModule({
  declarations: [
    WelcomeIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: WelcomeIndexComponent
      }
    ])
  ]
})
export class WelcomeModule {

}
