<?php

namespace Pickomenka\Models;

use DateTime;
use Pickomenka\Utils\ArrayUtils;
use Pickomenka\Utils\VerifyUtils;

class MatchModel
{
    private int $matchId;
    private ?int $parentMatchId1;
    private ?int $parentMatchId2;
    private ?int $teamId1;
    private ?int $teamId2;
    private ?int $winner;
    private ?DateTime $startDate;
    private int $matchType;
    private ?int $loserScore;
    private int $tournamentId;

    /**
     * @var MatchBetModel[]
     */
    private array $matchBet;

    public function __construct(int $matchId, ?int $parentMatch1, ?int $parentMatch2, ?int $team1, ?int $team2, ?int $winner, ?DateTime $startDate, int $matchType, ?int $loserScore, int $tournamentId) {
        $allParents = $parentMatch1 !== null && $parentMatch2 !== null;
        $partialParents = $parentMatch1 !== null || $parentMatch2 !== null;
        $allTeams = $team1 !== null && $team2 !== null;
        $partialTeams = $team1 !== null || $team2 !== null;

        if (($allTeams && $partialParents) || ($allParents && $partialTeams) || (!$allTeams && !$allParents)) {
            http_response_code(400);
            echo json_encode(['error_message' => 'A match must have either two teams or two parent matches, but not both.']);
            exit();
        }

        $this->matchId = $matchId;
        $this->parentMatchId1 = $parentMatch1;
        $this->parentMatchId2 = $parentMatch2;
        $this->teamId1 = $team1;
        $this->teamId2 = $team2;
        $this->winner = $winner;
        $this->startDate = $startDate;
        $this->matchType = $matchType;
        $this->loserScore = $loserScore;
        $this->tournamentId = $tournamentId;
        $this->matchBet = [];
    }

    /**
     * @return int
     */
    public function getMatchId(): int
    {
        return $this->matchId;
    }


    /**
     * @param int $matchId
     */
    public function setMatchId(int $matchId): void
    {
        $this->matchId = $matchId;
    }

    /**
     * @return int | null
     */
    public function getParentMatchId1(): int | null
    {
        return $this->parentMatchId1;
    }

    /**
     * @param int | null $parentMatchId1
     */
    public function setParentMatchId1(int | null $parentMatchId1): void
    {
        $this->parentMatchId1 = $parentMatchId1;
    }

    /**
     * @return int | null
     */
    public function getParentMatchId2(): int | null
    {
        return $this->parentMatchId2;
    }

    /**
     * @param int | null $parentMatchId2
     */
    public function setParentMatchId2(int | null $parentMatchId2): void
    {
        $this->parentMatchId2 = $parentMatchId2;
    }

    /**
     * @return int | null
     */
    public function getTeamId1(): int | null
    {
        return $this->teamId1;
    }

    /**
     * @param int | null $teamId1
     */
    public function setTeamId1(int | null $teamId1): void
    {
        $this->teamId1 = $teamId1;
    }

    /**
     * @return int | null
     */
    public function getTeamId2(): int | null
    {
        return $this->teamId2;
    }

    /**
     * @param int | null $teamId2
     */
    public function setTeamId2(int | null $teamId2): void
    {
        $this->teamId2 = $teamId2;
    }

    /**
     * @return int | null
     */
    public function getWinner(): int | null
    {
        return $this->winner;
    }

    /**
     * @param int | null $winner
     */
    public function setWinner(int | null $winner): void
    {
        $this->winner = $winner;
    }

    /**
     * @return DateTime | null
     */
    public function getStartDate(): DateTime | null
    {
        return $this->startDate;
    }

    /**
     * @param DateTime | null $startDate
     */
    public function setStartDate(DateTime | null $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return int
     */
    public function getMatchType(): int
    {
        return $this->matchType;
    }

    /**
     * @param int $matchType
     */
    public function setMatchType(int $matchType): void
    {
        $this->matchType = $matchType;
    }

    /**
     * @return int | null
     */
    public function getLoserScore(): int | null
    {
        return $this->loserScore;
    }

    /**
     * @param int | null $loserScore
     */
    public function setLoserScore(int | null $loserScore): void
    {
        $this->loserScore = $loserScore;
    }

    /**
     * @return int
     */
    public function getTournamentId(): int
    {
        return $this->tournamentId;
    }

    /**
     * @param int $tournamentId
     */
    public function setTournamentId(int $tournamentId): void
    {
        $this->tournamentId = $tournamentId;
    }

    public function isMatchDeterminedByParents(): bool
    {
        return $this->parentMatchId1 !== null && $this->parentMatchId2 !== null;
    }

    public function &getMatchBet(): array
    {
        return $this->matchBet;
    }

    public function addMatchBet(MatchBetModel $matchBet): void
    {
        $this->matchBet[] = $matchBet;
    }

    /**
     * @param array $matchBet
     */
    public function setMatchBet(array $matchBet): void
    {
        $this->matchBet = $matchBet;
    }

    public function toJson(): array
    {
        return [
            'matchId' => $this->matchId,
            'parentMatchId1' => $this->parentMatchId1,
            'parentMatchId2' => $this->parentMatchId2,
            'teamId1' => $this->teamId1,
            'teamId2' => $this->teamId2,
            'winner' => $this->winner,
            'startDate' => $this->startDate?->getTimestamp(),
            'matchType' => $this->matchType,
            'loserScore' => $this->loserScore,
            'tournamentId' => $this->tournamentId,
            'isMatchDeterminedByParents' => $this->isMatchDeterminedByParents()
        ];
    }

    public static function fromJson(array $json): MatchModel
    {
        list($matchType, $tournamentId, $parentMatchId1, $parentMatchId2, $teamId1, $teamId2, $winner, $startDate, $loserScore, $matchId) =
            ArrayUtils::checkArrayFields($json, ['matchType', 'tournamentId'], ['parentMatchId1', 'parentMatchId2', 'teamId1', 'teamId2', 'winner', 'startDate', 'loserScore', 'matchId']);

        $parentMatchId1 = VerifyUtils::verifyNumber($parentMatchId1, allowNull: true);
        $parentMatchId2 = VerifyUtils::verifyNumber($parentMatchId2, allowNull: true);
        $teamId1 = VerifyUtils::verifyNumber($teamId1, allowNull: true);
        $teamId2 = VerifyUtils::verifyNumber($teamId2, allowNull: true);
        $winner = VerifyUtils::verifyNumber($winner, allowNull: true);
        $startDate = VerifyUtils::verifyNumber($startDate, allowNull: true);
        $matchType = VerifyUtils::verifyNumber($matchType);
        $loserScore = VerifyUtils::verifyNumber($loserScore, allowZero: true, allowNull: true);
        $tournamentId = VerifyUtils::verifyNumber($tournamentId);
        $matchId = VerifyUtils::verifyNumber($matchId, allowNull: true);

        $startDate = $startDate === null ? null : DateTime::createFromFormat('U', $startDate);

        return new MatchModel($matchId ?? 0, $parentMatchId1, $parentMatchId2, $teamId1, $teamId2, $winner, $startDate, $matchType, $loserScore, $tournamentId);
    }
}
