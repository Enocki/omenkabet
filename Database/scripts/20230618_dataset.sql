START TRANSACTION;

INSERT INTO `tournaments` (`tournamentname`) VALUES ('LEC2023 - SummerSplit - Phase1 WE1');

INSERT INTO `teams` (`teamname`) VALUES ('MAD');
INSERT INTO `teams` (`teamname`) VALUES ('VIT');
INSERT INTO `teams` (`teamname`) VALUES ('TH');
INSERT INTO `teams` (`teamname`) VALUES ('SK');
INSERT INTO `teams` (`teamname`) VALUES ('KOI');
INSERT INTO `teams` (`teamname`) VALUES ('G2');
INSERT INTO `teams` (`teamname`) VALUES ('BDS');
INSERT INTO `teams` (`teamname`) VALUES ('FNC');
INSERT INTO `teams` (`teamname`) VALUES ('AST');
INSERT INTO `teams` (`teamname`) VALUES ('XL');

INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('1', '2', '1', '1');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('3', '4', '1', '1');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('5', '10', '1', '1');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('6', '7', '1', '1');
INSERT INTO `matchs` (`team1`, `team2`, `matchtype`, `tournamentid`) VALUES ('8', '9', '1', '1');

COMMIT;
