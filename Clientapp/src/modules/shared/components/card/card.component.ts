import {Component, Input} from '@angular/core';

@Component({
  selector: 'shared-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent {
  @Input()
  imgUrl: string = '';

  @Input()
  linkUrl: any[] = [];
}
