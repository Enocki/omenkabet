export class TournamentStats {
  public matchId: number;
  public team1: number;
  public teamName1: string;
  public users1: number;
  public team2: number;
  public teamName2: string;
  public users2: number;
  public statTeam1: number;
  public statTeam2: number;
  public winner?: number;
  public total: number;

  constructor(json: any) {
    console.log(json);

    this.matchId = json['matchId'];
    this.team1 = json['team1'];
    this.teamName1 = json['teamName1'];
    this.users1 = json['users1'];
    this.team2 = json['team2'];
    this.teamName2 = json['teamName2'];
    this.users2 = json['users2'];
    this.statTeam1 = json['statTeam1'];
    this.statTeam2 = json['statTeam2'];
    this.winner = json['winner'];
    this.total = json['total'];
  }

  public toJson(): any {
    return {
      'matchId': this.matchId,
      'team1': this.team1,
      'teamName1': this.teamName1,
      'users1': this.users1,
      'team2': this.team2,
      'teamName2': this.teamName2,
      'users2': this.users2,
      'statTeam1': this.statTeam1,
      'statTeam2': this.statTeam2,
      'winner': this.winner,
      'total': this.total,
    }
  }
}
