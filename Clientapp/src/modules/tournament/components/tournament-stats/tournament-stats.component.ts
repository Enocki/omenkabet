import {Component, Input, OnInit} from '@angular/core';
import {Tournament} from "../../../shared/models/tournament";
import {ApiService} from "../../../shared/services/api.service";
import {TournamentStats} from "../../../shared/models/tournament-stats";

@Component({
  selector: 'tournament-stats',
  templateUrl: './tournament-stats.component.html',
  styleUrls: ['./tournament-stats.component.less']
})
export class TournamentStatsComponent {
  @Input()
  set tournament (value: Tournament) {
    if (value.tournamentId)
      this.apiService.readTournamentStats(value.tournamentId!).then(stats => this.stats = stats);
  }

  public stats: TournamentStats[] = [];

  constructor(private apiService: ApiService) {
  }

}
