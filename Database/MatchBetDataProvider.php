<?php

namespace Pickomenka\Database;

use Pickomenka\Models\MatchBetModel;
use Pickomenka\Utils\SingletonTrait;

class MatchBetDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    public function createMatchbet(MatchBetModel $matchBet): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO matchbets (user, team, matchbets.match, loserscore) VALUES (?, ?, ?, ?)',
            [$matchBet->getUserId(), $matchBet->getTeamId(), $matchBet->getMatchId(), $matchBet->getLoserScore()]
        );

        $matchBet->setMatchBetId($result->lastInsertId);
    }

    public function readMatchBet(int $matchBetId): MatchBetModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT matchbetid, user, team, matchbets.match, loserscore FROM matchbets WHERE matchbetid = ?',
            [$matchBetId]
        );

        if ($rs === null)
            return null;

        return new MatchBetModel(
            $rs['matchbetid'],
            $rs['user'],
            $rs['team'],
            $rs['match'],
            $rs['loserscore']
        );
    }

    public function readMatchBetByUserIdAndMatchId(int $userId, int $matchId): MatchBetModel | null
    {
        $rs = Database::getInstance()->querySingle(
            'SELECT matchbetid, user, team, matchbets.match, loserscore FROM matchbets WHERE user = ? AND matchbets.match = ?',
            [$userId, $matchId]
        );

        if ($rs === null)
            return null;

        return new MatchBetModel(
            $rs['matchbetid'],
            $rs['user'],
            $rs['team'],
            $rs['match'],
            $rs['loserscore']
        );
    }

    /**
     * @param int $tournamentId
     * @return MatchBetModel[]
     */
    public function readMatchBetsByTournament(int $tournamentId): array
    {
        $rs = Database::getInstance()->queryAll(
            'SELECT matchbetid, user, team, matchbets.match, loserscore FROM matchbets WHERE matchbets.match IN (SELECT matchid FROM matchs WHERE tournamentid = ?)',
            [$tournamentId]
        );

        $matchBets = [];
        foreach ($rs as $row) {
            $matchBets[] = new MatchBetModel(
                $row['matchbetid'],
                $row['user'],
                $row['team'],
                $row['match'],
                $row['loserscore']
            );
        }

        return $matchBets;
    }

    public function updateMatchBet(MatchBetModel $matchBet): void
    {
        Database::getInstance()->update(
            'UPDATE matchbets SET user = ?, team = ?, matchbets.match = ?, loserscore = ? WHERE matchbetid = ?',
            [$matchBet->getUserId(), $matchBet->getTeamId(), $matchBet->getMatchId(), $matchBet->getLoserScore(), $matchBet->getMatchBetId()]
        );
    }

    public function deleteMatchBet(int $matchBetId): void
    {
        Database::getInstance()->update(
            'DELETE FROM matchbets WHERE matchbetid = ?',
            [$matchBetId]
        );
    }

}
