import {Component, ViewChild} from '@angular/core';
import {Team} from "../../shared/models/team";
import {ApiService} from "../../shared/services/api.service";
import {AdminStateService} from "../services/admin-state.service";
import {SaveButtonDirective} from "../../shared/directives/save-button.directive";

@Component({
  selector: 'admin-teams',
  templateUrl: './admin-teams.component.html'
})
export class AdminTeamsComponent {

  selectedTeam?: Team;
  emptyTeam: Team = new Team({ teamName: 'Nouvelle équipe' });

  @ViewChild('saveButton') saveButton?: SaveButtonDirective;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _stateService: AdminStateService,
  ) {
  }

  get teams(): Team[] { return this._stateService.teams; }

  get isNewTeam(): boolean { return this.selectedTeam?.teamId === undefined; }

  async saveTeam(): Promise<void> {
    if (this.selectedTeam === undefined)
      return;

    if (this.isNewTeam) {
      const newTeam = await this._apiService.createTeam(this.selectedTeam);
      this._stateService.teams?.push(newTeam);
      this.selectedTeam = newTeam;
    }
    else
      await this._apiService.updateTeam(this.selectedTeam);

    this.saveButton?.animate();
  }
}
