import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { FormsModule } from "@angular/forms";
import { PickomenkaButtonDirective } from './directives/pickomenka-button.directive';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { HttpClientModule } from "@angular/common/http";
import { CdnUrlPipe } from './pipes/cdn-url.pipe';
import {TournamentEditComponent} from "./components/tournament-edit/tournament-edit.component";
import {MatchEditComponent} from "./components/match-edit/match-edit.component";
import {MatchShowComponent} from "./components/match-show/match-show.component";
import {RouterModule} from "@angular/router";
import { TeamEditComponent } from './components/team-edit/team-edit.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SaveButtonDirective } from './directives/save-button.directive';



@NgModule({
  declarations: [
    CardComponent,
    PickomenkaButtonDirective,
    ProgressBarComponent,
    CdnUrlPipe,

    MatchEditComponent,
    MatchShowComponent,
    TeamEditComponent,
    SaveButtonDirective,
    SpinnerComponent,
    TournamentEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [
    CardComponent,
    PickomenkaButtonDirective,
    ProgressBarComponent,
    CommonModule,
    FormsModule,
    HttpClientModule,
    CdnUrlPipe,

    MatchEditComponent,
    MatchShowComponent,
    TeamEditComponent,
    SaveButtonDirective,
    SpinnerComponent,
    TournamentEditComponent
  ]
})
export class SharedModule { }
