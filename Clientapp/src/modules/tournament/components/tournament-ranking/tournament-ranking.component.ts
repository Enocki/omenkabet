import {Component, Input, OnInit} from '@angular/core';
import {Tournament} from "../../../shared/models/tournament";
import {ApiService} from "../../../shared/services/api.service";
import {User} from "../../../shared/models/user";

@Component({
  selector: 'tournament-ranking',
  templateUrl: './tournament-ranking.component.html',
  styleUrls: ['./tournament-ranking.component.less'],
  host: {
    class: 'flex-row justify-content-center align-items-center'
  }
})
export class TournamentRankingComponent implements OnInit {
  @Input()
  set tournament(value: Tournament) {
    if (value.tournamentId)
      this._apiService.readTournamentRanking(value.tournamentId)
        .then(
          ranking => this.ranking = ranking.sort((a, b) => b.points - a.points)
        )
  }

  ranking: { user: User, points: number }[] = [];

  constructor(
    private readonly _apiService: ApiService
  ) {}

  async ngOnInit(): Promise<void> {
  }
}
