import {Injectable} from '@angular/core';
import {User} from "../models/user";
import {Match} from "../models/match";
import {MatchBet} from "../models/match-bet";
import {Team} from "../models/team";
import {Tournament} from "../models/tournament";
import {TournamentBet} from "../models/tournament-bet";
import {ApiError} from "../models/api-error";
import {SettingsService} from "./settings.service";
import {TournamentStats} from "../models/tournament-stats";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly API_BASE_URL: string;

  constructor(settingsService: SettingsService) {
    this.API_BASE_URL = settingsService.API_BASE_URL;
  }

  private async _tryParseJsonResponse(response: Response): Promise<any> {
    try {
      return await response.json();
    }
    catch (e: any) {
      return undefined;
    }
  }

  //#region User
  public async readUser(): Promise<User> {
    const response = await fetch(this.API_BASE_URL + 'user', {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the user', response.status);

    return new User(await response.json());
  }

  public async login(login:string, password: string): Promise<User> {
    const response = await fetch(this.API_BASE_URL + 'login', {
      method: 'POST',
        credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          'login': login,
          'password': password
        }
      )
    });

    if (!response.ok)
      throw new ApiError('Unable to log in', response.status, await this._tryParseJsonResponse(response));

    return new User(await response.json());
  }

  public async updateUserAvatar(avatar: string, extension: string): Promise<string> {
    const response = await fetch(this.API_BASE_URL + 'user/avatar', {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          'avatar': avatar,
          'extension': extension
        }
      )
    });

    if (!response.ok)
      throw new ApiError('Unable to update user avatar', response.status, await this._tryParseJsonResponse(response));

    return (await response.json())['avatar'];
  }

  public async logout(): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'user', {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to logout', response.status);
  }

  public async register(email: string, username: string, password: string): Promise<User> {
    const response = await fetch(this.API_BASE_URL + 'register', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'email': email,
        'username': username,
        'password': password
      })
    });

    if (!response.ok)
      throw new ApiError('Unable to register', response.status, await this._tryParseJsonResponse(response));

    return new User(await response.json());
  }
  //#endregion

  //#region Match
  public async readMatch(matchId: number): Promise<Match> {
    const response = await fetch(this.API_BASE_URL + 'match/' + matchId, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the match', response.status);

    return new Match(await response.json());
  }

  public async createMatch(match: Match): Promise<Match> {
    const response = await fetch(this.API_BASE_URL + 'match', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(match.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to create the match ', response.status);

    return new Match(await response.json());
  }

  public async updateMatch(match: Match): Promise<Match> {
    const response = await fetch(this.API_BASE_URL + 'match/' + match.matchId, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(match.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to update the match', response.status);

    return new Match(await response.json());
  }

  public async deleteMatch(matchId: number): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'match/' + matchId, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to delete the match', response.status);
  }
  //#endregion

  //#region MatchBet
  public async createMatchBet(matchBet: MatchBet): Promise<MatchBet> {
    const response = await fetch(this.API_BASE_URL + 'matchbet', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(matchBet.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to create the match bet', response.status);

    return new MatchBet(await response.json());
  }

  public async updateMatchBet(matchBet: MatchBet): Promise<MatchBet> {
    const response = await fetch(this.API_BASE_URL + 'matchbet/' + matchBet.matchBetId, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(matchBet.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to update the match bet', response.status);

    return new MatchBet(await response.json());
  }

  public async deleteMatchBet(matchBetId: number): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'matchbet/' + matchBetId, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to delete the match bet', response.status);
  }
  //#endregion

  //#region Team

  public async readTeam(teamId: number): Promise<Team> {
    const response = await fetch(this.API_BASE_URL + 'team/' + teamId, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the team', response.status);

    return new Team(await response.json());
  }

  public async readTeams(): Promise<Team[]> {
    const response = await fetch(this.API_BASE_URL + 'teams', {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the team', response.status);

    return (await response.json()).map((data: any) => new Team(data));
  }

  public async createTeam(team: Team): Promise<Team> {
    const response = await fetch(this.API_BASE_URL + 'team', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(team.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to create the team', response.status);

    return new Team(await response.json());
  }

  public async updateTeam(team: Team): Promise<Team> {
    const response = await fetch(this.API_BASE_URL + 'team/' + team.teamId, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(team.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to update the team', response.status);

    return new Team(await response.json());
  }

  public async updateTeamAvatar(teamId: number, avatar: string, extension: string): Promise<string> {
    const response = await fetch(this.API_BASE_URL + 'team/' + teamId + '/avatar', {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          'avatar': avatar,
          'extension': extension
        }
      )
    });

    if (!response.ok)
      throw new ApiError('Unable to update team avatar', response.status, await this._tryParseJsonResponse(response));

    return (await response.json())['avatar'];
  }

  public async deleteTeam(teamId: number): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'team/' + teamId, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to delete the team', response.status);
  }

  //#endregion

  //#region Tournament
  public async readTournament(tournamentId: number): Promise<Tournament> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournamentId, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the tournament', response.status);

    return new Tournament(await response.json());
  }

  public async readNextTournament(): Promise<Tournament | undefined> {
    try {
      const response = await fetch(this.API_BASE_URL + 'tournament/next', {
        method: 'GET',
        credentials: 'include',
        headers: {
          'Accept': '*/*'
        }
      });

      if (!response.ok)
        throw new ApiError('Unable to read the tournament', response.status);

      return new Tournament(await response.json());
    }
    catch (error) {
      if (error instanceof ApiError && error.statusCode === 404)
        return undefined;
      else
        throw error;
    }
  }

  public async readTournamentRanking(tournamentId: number): Promise<{ user: User, points: number }[]> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournamentId + '/ranking', {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the tournament ranking', response.status);

    return (await response.json()).map((data: any) => {
      return {
        user: new User(data.user),
        points: data.points
      };
    });
  }

  public async readTournaments(withDetails: boolean = false): Promise<Tournament[]> {
    const response = await fetch(this.API_BASE_URL + 'tournaments?withDetails=' + withDetails, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to read the tournament', response.status);

    return (await response.json()).map((data: any) => new Tournament(data));
  }

  public async readTournamentStats(tournamentId: number): Promise<TournamentStats[]> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournamentId + '/stats', {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to get stats tournament', response.status);

    return (await response.json()).map( (data: any) => new TournamentStats(data));
  }


  public async createTournament(tournament: Tournament): Promise<Tournament> {
    const response = await fetch(this.API_BASE_URL + 'tournament', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(tournament.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to create the tournament', response.status);

    return new Tournament(await response.json());
  }

  public async updateTournament(tournament: Tournament): Promise<Tournament> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournament.tournamentId, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(tournament.toJson())
    });

    if (!response.ok)
      throw new ApiError('Unable to update the tournament', response.status);

    return new Tournament(await response.json());
  }

  public async updateTournamentAvatar(tournamentId: number, avatar: string, extension: string): Promise<string> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournamentId + '/avatar', {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          'avatar': avatar,
          'extension': extension
        }
      )
    });

    if (!response.ok)
      throw new ApiError('Unable to update tournament avatar', response.status, await this._tryParseJsonResponse(response));

    return (await response.json())['avatar'];
  }

  public async deleteTournament(tournamentId: number): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'tournament/' + tournamentId, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to delete the tournament', response.status);
  }

  //#endregion

  //#region TournamentBet
  public async createTournamentBet(tournamentBet: TournamentBet): Promise<TournamentBet> {
    const response = await fetch(this.API_BASE_URL + 'tournamentbet', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to create the tournament bet', response.status);

    return new TournamentBet(await response.json());
  }

  public async updateTournamentBet(tournamentBet: TournamentBet): Promise<TournamentBet> {
    const response = await fetch(this.API_BASE_URL + 'tournamentbet/' + tournamentBet.tournamentBetId, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Accept' : '*/*',
        'Content-Type': 'application/json'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to update the tournament bet', response.status);

    return new TournamentBet(await response.json());
  }

  public async deleteTournamentBet(tournamentBetId: number): Promise<void> {
    const response = await fetch(this.API_BASE_URL + 'tournamentbet/' + tournamentBetId, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Accept' : '*/*'
      }
    });

    if (!response.ok)
      throw new ApiError('Unable to delete the tournament bet', response.status);
  }

  //#endregion
}
