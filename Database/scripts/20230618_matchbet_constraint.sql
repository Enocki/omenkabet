ALTER TABLE `matchbets`
    ADD UNIQUE INDEX `uq_matchbets_usermatch` (`user` ASC, `match` ASC) VISIBLE;
;
