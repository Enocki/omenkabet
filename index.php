<?php

namespace Pickomenka;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Pickomenka\Controllers\LoginController;
use Pickomenka\Controllers\MatchBetController;
use Pickomenka\Controllers\MatchController;
use Pickomenka\Controllers\RankingUsersController;
use Pickomenka\Controllers\RegisterController;
use Pickomenka\Controllers\Team\TeamAvatarController;
use Pickomenka\Controllers\Team\TeamController;
use Pickomenka\Controllers\TeamsController;
use Pickomenka\Controllers\Tournament\TournamentAvatarController;
use Pickomenka\Controllers\Tournament\TournamentBetController;
use Pickomenka\Controllers\Tournament\TournamentController;
use Pickomenka\Controllers\Tournament\TournamentNextController;
use Pickomenka\Controllers\Tournament\TournamentRankingController;
use Pickomenka\Controllers\Tournament\TournamentsController;
use Pickomenka\Controllers\Tournament\TournamentStatsController;
use Pickomenka\Controllers\User\UserAvatarController;
use Pickomenka\Controllers\User\UserController;
use Pickomenka\Exceptions\InternalErrorException;
use function FastRoute\simpleDispatcher;

require_once 'vendor/autoload.php';
require_once 'Config/config.php';
global $CONFIG;

session_start();
header('Access-Control-Allow-Origin: ' . $CONFIG['allowed_origin']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS');
header('Access-Control-Allow-Headers: Accept, Content-Type, Cookie, User-Agent');

$dispatcher = simpleDispatcher(function(RouteCollector $r) {
    $r->addRoute(['POST'], 'login', LoginController::class);

    $r->addRoute(['GET', 'POST', 'PUT', 'DELETE'], 'match[/{id:\d+}]', MatchController::class);

    $r->addRoute(['POST', 'PUT', 'DELETE'], 'matchbet[/{id:\d+}]', MatchBetController::class);

    $r->addRoute(['GET'], 'ranking-users', RankingUsersController::class);

    $r->addRoute(['POST'], 'register', RegisterController::class);

    $r->addRoute(['GET', 'POST', 'PUT', 'DELETE'], 'team[/{id:\d+}]', TeamController::class);
    $r->addRoute(['PUT'], 'team/{id:\d+}/avatar', TeamAvatarController::class);
    $r->addRoute(['GET'], 'teams', TeamsController::class);

    $r->addRoute(['GET', 'POST', 'PUT', 'DELETE'], 'tournament-bet[/{id:\d+}]', TournamentBetController::class);

    $r->addRoute(['GET', 'POST', 'PUT', 'DELETE'], 'tournament[/{id:\d+}]', TournamentController::class);
    $r->addRoute(['PUT'], 'tournament/{id:\d+}/avatar', TournamentAvatarController::class);
    $r->addRoute(['GET'], 'tournament/next', TournamentNextController::class);
    $r->addRoute(['GET'], 'tournament/{id:\d+}/ranking', TournamentRankingController::class);
    $r->addRoute(['GET'], 'tournament/{id:\d+}/stats', TournamentStatsController::class);

    $r->addRoute(['GET'], 'tournaments', TournamentsController::class);

    $r->addRoute(['GET', 'DELETE'], 'user', UserController::class);
    $r->addRoute(['GET', 'PUT', 'DELETE'], 'user/avatar', UserAvatarController::class);
});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = substr($_SERVER['REQUEST_URI'], strlen($CONFIG['uri_prefix']));

// Remove query params and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);


$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case Dispatcher::NOT_FOUND:
        http_response_code(404);
        var_dump($uri);
        break;

    case Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        $queryParams = [];
        parse_str($_SERVER['QUERY_STRING'], $queryParams);

        try {
            header('Content-Type: application/json; charset=utf-8');
            $controller = new $handler($vars, $queryParams);
            if (call_user_func([$controller, strtolower($httpMethod)]) === false) {
                http_response_code(500);
                exit();
            }
        } catch (InternalErrorException $e) {
            http_response_code(500);
            if (!$CONFIG['production_mode']) {
                header('Content-Type: text/html; charset=utf-8');
                var_dump($e->getReason());
            }
            exit();
        }

        break;

    case Dispatcher::METHOD_NOT_ALLOWED:
        header('Allow: ' . join(', ', $routeInfo[1]));

        // Normal case if it's an option request
        if ($httpMethod === 'OPTIONS') {
            http_response_code(204);
            exit();
        }

        // Otherwise bad request
        http_response_code(405);

        break;
}

