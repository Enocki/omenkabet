<?php

namespace Pickomenka\Controllers\Team;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Utils\CdnUtils;
use Pickomenka\Utils\VerifyUtils;

class TeamAvatarController extends AbstractController
{
    public function put(): void
    {
        $this->ensureAdmin();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $existingTeam = TeamDataProvider::getInstance()->readTeam($id);
        if ($existingTeam === null)
            $this->notFound();

        list($avatar, $avatarExtension) = $this->jsonBody(['avatar', 'extension']);

        if (!in_array($avatarExtension, CdnUtils::getInstance()->getAllowedAvatarExtensions()))
            $this->badRequest('Invalid avatar extension.');

        if ($existingTeam->getAvatar() !== null)
            CdnUtils::getInstance()->deleteFromCdn($existingTeam->getAvatar());

        $newAvatar = CdnUtils::getInstance()->postToCdn(CdnUtils::getInstance()->getMimeTypeByExtension()[$avatarExtension], $avatar);
        $existingTeam->setAvatar($newAvatar);

        TeamDataProvider::getInstance()->updateTeam($existingTeam, true);

        echo json_encode([ 'avatar' => $newAvatar ]);
    }
}