<?php

namespace Pickomenka\Database;

use Pickomenka\Exceptions\InternalErrorException;
use Pickomenka\Utils\SingletonTrait;
use PDO;

class Database
{
    use SingletonTrait;

    private readonly bool $productionMode;

    private readonly PDO $pdo;
    private bool $transactionInProgress;

    /**
     * @throws InternalErrorException
     */
    public function __construct()
    {
        global $CONFIG;

        $this->productionMode = $CONFIG['production_mode'];

        $host = $CONFIG['database_host'];
        $dbname = $CONFIG['database_dbname'];
        $user = $CONFIG['database_user'];
        $password = $CONFIG['database_password'];

        try {
            $this->pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            $this->transactionInProgress = false;
        }
        catch (\PDOException $e) {
            http_response_code(500);
            if (!$this->productionMode)
                throw new InternalErrorException($e);
            exit();
        }
    }

    public function beginTransaction(): void
    {
        if ($this->transactionInProgress)
            return;

        $this->pdo->beginTransaction();
        $this->transactionInProgress = true;
    }

    public function commit(): void
    {
        if (!$this->transactionInProgress)
            return;

        $this->pdo->commit();
        $this->transactionInProgress = false;
    }

    /**
     * @throws InternalErrorException
     */
    function queryAll(string $query, array $params): array
    {
        try
        {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($params) === false)
            {
                http_response_code(500);
                if (!$this->productionMode)
                    throw new InternalErrorException($this->pdo->errorInfo());
                exit();
            }

            $results = $stmt->fetchAll();
            if ($results === false)
            {
                http_response_code(500);
                if (!$this->productionMode)
                    throw new InternalErrorException($stmt);
                exit();
            }

            return $results;
        }
        catch (\PDOException $e)
        {
            http_response_code(500);
            if (!$this->productionMode)
                throw new InternalErrorException($e);
            exit();
        }
    }

    /**
     * @throws InternalErrorException
     */
    function querySingle(string $query, array $params): null | array
    {
        $rows = $this->queryAll($query, $params);
        $rowAmount = count($rows);

        if ($rowAmount > 1)
        {
            http_response_code(500);
            if (!$this->productionMode)
                throw new InternalErrorException(['Query single found multiple rows', $query, $params]);
            exit();
        }

        return $rowAmount === 0 ? null : $rows[0];
    }

    /**
     * @throws InternalErrorException
     */
    function update(string $query, array $params): DatabaseUpdateResult {
        foreach (array_keys($params) as $paramKey)
        {
            $param = $params[$paramKey];
            if (gettype($param) === 'boolean')
                $params[$paramKey] = $param ? 1 : 0;
        }

        try
        {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($params) === false)
            {
                http_response_code(500);
                if (!$this->productionMode)
                    throw new InternalErrorException($this->pdo->errorInfo());
                exit();
            }

            return new DatabaseUpdateResult($stmt->rowCount(), $this->pdo->lastInsertId());
        }
        catch (\PDOException $e)
        {
            http_response_code(500);
            if (!$this->productionMode)
                throw new InternalErrorException($e);
            exit();
        }
    }
}