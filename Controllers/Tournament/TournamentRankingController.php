<?php

namespace Pickomenka\Controllers\Tournament;

use Pickomenka\Controllers\AbstractController;
use Pickomenka\Database\MatchBetDataProvider;
use Pickomenka\Database\MatchDataProvider;
use Pickomenka\Database\TeamDataProvider;
use Pickomenka\Database\TournamentDataProvider;
use Pickomenka\Database\UserDataProvider;
use Pickomenka\Utils\MatchUtils;
use Pickomenka\Utils\VerifyUtils;

class TournamentRankingController extends AbstractController
{
    public function get(): void
    {
        $this->ensureAuthentication();

        $rawId = $this->pathVars['id'];
        $id = VerifyUtils::verifyNumber($rawId);

        $tournament = TournamentDataProvider::getInstance()->readTournament($id);
        if ($tournament === null)
            $this->notFound();

        $teams = TeamDataProvider::getInstance()->readTeamsByTournament($id);
        $matches = MatchDataProvider::getInstance()->readMatchesByTournament($id);
        $matchBets = MatchBetDataProvider::getInstance()->readMatchBetsByTournament($id);

        $winners = [];
        foreach ($matches as $match)
            $winners[$match->getMatchId()] = MatchUtils::getMatchWinner($match, $matches, $teams);

        $points = [];
        foreach ($matchBets as $matchBet) {
            if (!isset($winners[$matchBet->getMatchId()]))
                continue;

            $currentPoints = $points[$matchBet->getUserId()] ?? 0;
            if ($winners[$matchBet->getMatchId()]->getTeamId() === $matchBet->getTeamId())
                $points[$matchBet->getUserId()] = $currentPoints + 1;
        }

        if (count($points) === 0) {
            echo json_encode([]);
            exit;
        }

        foreach (UserDataProvider::getInstance()->readUsers(array_keys($points)) as $user) {
            $points[$user->getUserId()] = [
                'user' => $user->toJson(true),
                'points' => $points[$user->getUserId()]
            ];
        }

        echo json_encode(array_values($points));
    }
}