import {Component} from '@angular/core';
import {AuthenticationService} from "../../shared/services/authentication.service";
import {User} from "../../shared/models/user";
import {ApiService} from "../../shared/services/api.service";

@Component({
  selector: 'profile-index',
  templateUrl: './profile-index.component.html',
  styleUrls: [
    'profile-index.component.less'
  ],
  host: {
    class: 'flex-column align-items-center'
  }
})
export class ProfileIndexComponent {
  constructor(
    private readonly _apiService: ApiService,
    private readonly _authenticationService: AuthenticationService
  ) {}

  isUploadInProgress = false;

  get me(): User {
    return this._authenticationService.currentUser ?? new User({});
  }

  async handleAvatarChange(event: Event) {
    const input = event.target;
    if (!(input instanceof HTMLInputElement))
      return;

    const file = input.files?.item(0);
    if (!file)
      return;

    const me = this;

    const nameParts = file.name.split('.');
    const extension = nameParts[nameParts.length - 1];
    if (nameParts.length < 2 || (extension !== 'png' && extension !== 'jpg' && extension !== 'jpeg' && extension !== 'gif')) {
      alert('Invalid file type. Only PNG, JPG and GIF are supported.');
      input.value = '';
      return;
    }

    this.isUploadInProgress = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = async function () {
      input.value = '';

      const result = reader.result as string;
      const base64 = result.split(',')[1];

      me._authenticationService.currentUser!.avatar = await me._apiService.updateUserAvatar(base64, extension);
      me.isUploadInProgress = false;
    };
    reader.onerror = function (error) {
      console.error('Error: ', error);
    };
  }
}
