import {NgModule} from '@angular/core';
import { LogoutIndexComponent } from './components/logout-index.component';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    LogoutIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LogoutIndexComponent
      }
    ])
  ]
})
export class LogoutModule {}

