<?php

namespace Pickomenka\Controllers;

use JetBrains\PhpStorm\NoReturn;
use Pickomenka\Database\UserDataProvider;
use Pickomenka\Models\UserModel;
use Pickomenka\Utils\ArrayUtils;

abstract class AbstractController
{
    protected readonly array $pathVars;
    protected readonly array $queryVars;

    private ?UserModel $currentUser;


    /**
     * @param array $pathVars
     * @param array $queryVars
     */
    public function __construct(array $pathVars, array $queryVars)
    {
        $this->pathVars = $pathVars;
        $this->queryVars = $queryVars;
        $this->currentUser = null;
    }

    //#region Authentication

    final protected function ensureAuthentication(): void
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        // Try to authenticate with cookies
        if (isset($_COOKIE['omenkabettoken']))
        {
            $token = urldecode($_COOKIE['omenkabettoken']);

            $existingUser = UserDataProvider::getInstance()->readUserByToken($token);
            if ($existingUser !== null)
            {
                $_SESSION['userid'] = $existingUser->getUserId();
                $this->updateAuthCookies($token);
                $this->currentUser = $existingUser;
                return;
            }

            $this->updateAuthCookies(null);
        }

        if (isset($_SESSION['userid'])) {
            if ($this->currentUser === null)
                $this->currentUser = UserDataProvider::getInstance()->readUser($_SESSION['userid']);
            return;
        }

        session_destroy();
        http_response_code(401);
        exit();
    }

    final protected function ensureAdmin(): void
    {
        $this->ensureAuthentication();
        if ($this->currentUser->getRole() !== 1)
            $this->forbidden();
    }

    /**
     * Update (or renew) the authentication cookies
     * @param ?string $token The user token to set
     * @return void
     */
    final protected function updateAuthCookies(?string $token): void
    {
        setcookie('omenkabettoken', urlencode($token ?? ''), [
            'expires' => $token == null ? -1 : time() + 60 * 60 * 24 * 30 * 11,
            'httponly' => true,
            'samesite' => 'strict',
            'secure' => true
        ]);
    }

    /**
     * Get the id of the logged user (check if the user is logged first)
     * @return int The id of the logged user
     */
    final protected function getLoggedUserId(): int
    {
        $this->ensureAuthentication();
        return $_SESSION['userid'];
    }

    /**
     * Fetch the logged user (check if the user is logged first)
     * @return UserModel The logged user
     */
    final protected function getLoggedUser(): UserModel
    {
        $this->ensureAuthentication();
        return $this->currentUser;
    }

    //#endregion

    //#region Body helpers

    /**
     * Get the JSON raw content sent by the client
     *
     * @param string[] $mandatoryFields The fields that must be present in the JSON (otherwise a 400 error is returned)
     * @param string[] $optionalFields The optional fields that can be present in the JSON
     * @return array The JSON content in the same order as the mandatory fields concatenated with the optional fields
     */
    final protected function jsonFromRaw(string $content, array $mandatoryFields = [], array $optionalFields = []): array {
        $decoded = strlen($content) === 0 ? [] : json_decode($content, true);
        if ($decoded === null)
            $this->badRequest("Invalid JSON content");

        return ArrayUtils::checkArrayFields($decoded, $mandatoryFields, $optionalFields);
    }

    /**
     * Get the JSON content sent by the client
     *
     * @param string[] $mandatory_fields The fields that must be present in the JSON (otherwise a 400 error is returned)
     * @param string[] $optional_fields The optional fields that can be present in the JSON
     * @return array The JSON content in the same order as the mandatory fields concatenated with the optional fields
     */
    final protected function jsonBody(array $mandatory_fields = [], array $optional_fields = []): array {
        $content = file_get_contents('php://input');
        if ($content === false)
            $this->badRequest("Invalid JSON content");

        return $this->jsonFromRaw($content, $mandatory_fields, $optional_fields);
    }

    //#endregion

    //#region HTTP response helpers

    #[NoReturn] final protected function ok(mixed $data): void
    {
        http_response_code(200);
        echo json_encode($data);
        exit();
    }

    #[NoReturn] final protected function created(mixed $data): void
    {
        http_response_code(201);
        echo json_encode($data);
        exit();
    }

    #[NoReturn] final protected function noContent(): void
    {
        http_response_code(204);
        exit();
    }

    #[NoReturn] final protected function badRequest(string $reason): void
    {
        http_response_code(400);
        echo json_encode(['status_code' => 400, 'error_message' => $reason]);
        exit();
    }

    #[NoReturn] final protected function unauthorized(string $reason): void
    {
        http_response_code(401);
        exit();
    }

    #[NoReturn] final protected function forbidden(): void
    {
        http_response_code(403);
        exit();
    }

    #[NoReturn] final protected function notFound(): void
    {
        http_response_code(404);
        exit();
    }

    #[NoReturn] final protected function internalServerError(string $reason): void
    {
        http_response_code(500);
        echo json_encode(['status_code' => 500, 'error_message' => $reason]);
        exit();
    }

    //#endregion

    public function post(): void {}
    public function head(): void {}
    public function get(): void {}
    public function put(): void {}
    public function patch(): void {}
    public function delete(): void {}
}