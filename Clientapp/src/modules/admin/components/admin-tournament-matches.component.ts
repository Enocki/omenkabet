import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Tournament} from "../../shared/models/tournament";
import {EMatchType, Match} from "../../shared/models/match";
import {Team} from "../../shared/models/team";
import {AdminStateService} from "../services/admin-state.service";
import {Subscription} from "rxjs";
import {ApiService} from "../../shared/services/api.service";
import {MatchService} from "../../shared/services/match.service";
import {SaveButtonDirective} from "../../shared/directives/save-button.directive";

@Component({
  selector: 'admin-tournament-matches',
  templateUrl: './admin-tournament-matches.component.html'
})
export class AdminTournamentMatchesComponent implements OnInit, OnDestroy {
  selectedMatch?: Match;
  emptyMatch?: Match;

  @ViewChild('saveButton') saveButton?: SaveButtonDirective;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _matchService: MatchService,
    private readonly _stateService: AdminStateService,
  ) {}

  private _subscription?: Subscription;
  ngOnInit(): void {
    this._subscription = this._stateService.tournamentChangeObservable.subscribe((_: Tournament | undefined) => {
      this.selectedMatch = undefined;

      if (!!this._stateService.tournament && !!this._stateService.tournament.tournamentId)
        this.emptyMatch = new Match({
          matchType: EMatchType.BO1,
          isMatchDeterminedByParents: false,
          tournamentId: this._stateService.tournament.tournamentId
        });
    });

    if (!!this._stateService.tournament && !!this._stateService.tournament.tournamentId)
      this.emptyMatch = new Match({
        matchType: EMatchType.BO1,
        isMatchDeterminedByParents: false,
        tournamentId: this._stateService.tournament.tournamentId
      });
  }

  ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  getMatchTeam1(match: Match): Team | undefined {
    return this._matchService.getMatchTeam1(match, this.matches, this.teams);
  }

  getMatchTeam2(match: Match): Team | undefined {
    return this._matchService.getMatchTeam2(match, this.matches, this.teams);
  }

  get matches(): Match[] { return this._stateService.tournament?.matches ?? []; }
  get teams(): Team[] { return this._stateService.teams; }

  get isNewMatch(): boolean { return this.selectedMatch?.matchId === undefined; }

  async saveMatch(): Promise<void> {
    if (this.selectedMatch === undefined)
      return;

    if (this.isNewMatch) {
      const newMatch = await this._apiService.createMatch(this.selectedMatch);
      this._stateService.tournament?.matches.push(newMatch);
      this.selectedMatch = newMatch;
    }
    else
      await this._apiService.updateMatch(this.selectedMatch);

    this.saveButton?.animate();
  }
}
