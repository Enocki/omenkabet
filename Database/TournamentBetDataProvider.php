<?php

namespace Pickomenka\Database;

use Pickomenka\Models\TournamentBetModel;
use Pickomenka\Utils\SingletonTrait;

class TournamentBetDataProvider
{
    use SingletonTrait;

    protected function __construct()
    {
    }

    public function createTournamentBet(TournamentBetModel $tournamentBet): void
    {
        $result = Database::getInstance()->update(
            'INSERT INTO tournamentbets (team, user, tournament) VALUES (?, ?, ?)',
            [$tournamentBet->getTeam(), $tournamentBet->getUser(), $tournamentBet->getTournament()]
        );

        $tournamentBet->setTournamentBetId($result->lastInsertId);
    }

    public function readTournamentBet(int $tournamentBetId): TournamentBetModel | null
    {
        $rs = Database::getInstance()->querySingle(
          'SELECT tournamentbetid, team, user, tournament FROM tournamentbets WHERE tournamentbetid = ?',
            [$tournamentBetId]
        );

        if ($rs === null)
            return null;

        return new TournamentBetModel(
          $rs['tournamentbetid'],
          $rs['team'],
          $rs['user'],
          $rs['tournament']
        );
    }

    public function updateTournamentBet(TournamentBetModel $tournamentBet): void
    {
        Database::getInstance()->update(
          'UPDATE tournamentbets SET team = ?, user = ?, tournament = ? WHERE tournamentbetid = ?',
            [$tournamentBet->getTeam(), $tournamentBet->getUser(), $tournamentBet->getTournament(), $tournamentBet->getTournamentBetId()]
        );
    }

    public function deleteTournamentBet(int $tournamentBetId): void
    {
        Database::getInstance()->update(
            'DELETE FROM tournamentbets WHERE tournamentbetid = ?',
            [$tournamentBetId]
        );
    }

}