import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RankingIndexComponent } from './components/ranking-index.component';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    RankingIndexComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: RankingIndexComponent
      }
    ])
  ]
})
export class RankingModule { }
