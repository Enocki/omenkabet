<?php

namespace Pickomenka\Models;

use Pickomenka\Utils\ArrayUtils;
use Pickomenka\Utils\VerifyUtils;

class MatchBetModel
{
    private ?int $matchBetId;
    private int $userId;
    private int $teamId;
    private int $matchId;
    private ?int $loserScore;

    public function __construct(?int $matchBetId, int $userId, int $teamId, int $matchId, ?int $loserScore)
    {
        $this->matchBetId = $matchBetId;
        $this->userId = $userId;
        $this->teamId = $teamId;
        $this->matchId = $matchId;
        $this->loserScore = $loserScore;
    }

    /**
     * @return ?int
     */
    public function getMatchBetId(): ?int
    {
        return $this->matchBetId;
    }

    /**
     * @param int $matchBetId
     */
    public function setMatchBetId(int $matchBetId): void
    {
        $this->matchBetId = $matchBetId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return int
     */
    public function getMatchId(): int
    {
        return $this->matchId;
    }

    /**
     * @param int $matchId
     */
    public function setMatchId(int $matchId): void
    {
        $this->matchId = $matchId;
    }

    /**
     * @return int|null
     */
    public function getLoserScore(): ?int
    {
        return $this->loserScore;
    }

    /**
     * @param int|null $loserScore
     */
    public function setLoserScore(?int $loserScore): void
    {
        $this->loserScore = $loserScore;
    }

    public function toJson(): array
    {
        return [
            'matchBetId' => $this->matchBetId,
            'userId' => $this->userId,
            'teamId' => $this->teamId,
            'matchId' => $this->matchId,
            'loserScore' => $this->loserScore
        ];
    }

    public static function fromJson(array $json, int $userId): MatchBetModel
    {
        list ($teamId, $matchId, $matchBetId, $loserScore) = ArrayUtils::checkArrayFields($json, ['teamId', 'matchId'], ['matchBetId', 'loserScore']);

        $teamId = VerifyUtils::verifyNumber($teamId);
        $matchId = VerifyUtils::verifyNumber($matchId);
        $loserScore = VerifyUtils::verifyNumber($loserScore, allowNull: true);
        $matchBetId = VerifyUtils::verifyNumber($matchBetId, allowNull: true);

        return new MatchBetModel($matchBetId ?? 0, $userId, $teamId, $matchId, $loserScore);
    }
}