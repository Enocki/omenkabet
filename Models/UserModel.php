<?php

namespace Pickomenka\Models;

use Pickomenka\Utils\ArrayUtils;

class UserModel
{
    private int $userId;
    private ?string $authToken;
    private ?string $avatar;
    private string $email;
    private string $password;
    private int $role;
    private string $userName;


    public function __construct(int $userId, ?string $authToken, ?string $avatar, string $email, string $password, int $role, string $username) {
        $this->userId = $userId;
        $this->authToken = $authToken;
        $this->avatar = $avatar;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
        $this->userName = $username;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getAuthToken(): ?string
    {
        return $this->authToken;
    }

    /**
     * @param string|null $authToken
     */
    public function setAuthToken(?string $authToken): void
    {
        $this->authToken = $authToken;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(?string $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getRole(): int
    {
        return $this->role;
    }

    public function isAdmin(): bool
    {
        return $this->role & 1 > 0;
    }

    /**
     * @param int $role
     */
    public function setRole(int $role): void
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return array
     */
    public function toJson(bool $lite = false): array
    {
        $rs = [
            'userId' => $this->userId,
            'avatar' => $this->avatar,
            'userName' => $this->userName,
        ];

        if (!$lite) {
            $rs['email'] = $this->email;
            $rs['isAdmin'] = $this->isAdmin();
        }

        return $rs;
    }

    public static function fromJson(array $json): UserModel
    {
        list($userId, $avatar, $email, $isAdmin, $userName) = ArrayUtils::checkArrayFields($json, ['userId', 'avatar', 'email', 'isAdmin', 'userName'], []);
        $role = 0;
        if ($isAdmin)
            $role = $role | 1;

        return new UserModel($userId, null, $avatar, $email, '', $role, $userName);
    }

}
