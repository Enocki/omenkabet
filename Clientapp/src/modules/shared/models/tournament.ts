import {Match} from "./match";
import {Team} from "./team";
import {MatchBet} from "./match-bet";

export interface ITournament {
  tournamentId?: number;
  avatar?: string;
  tournamentName: string;
  startDate?: number;
  endDate?: number;

  previousTournamentId?: number;
  nextTournamentId?: number;

  matches?: Match[];
  teams?: Team[];
  matchBets?: MatchBet[];
}

export class Tournament {
  public tournamentId?: number;
  public avatar?: string;
  public tournamentName: string;
  public startDate?: Date;
  public endDate?: Date;

  public previousTournamentId?: number;
  public nextTournamentId?: number;

  public matches: Match[] = [];
  public teams: Team[] = [];
  public matchBets: MatchBet[] = [];

  constructor(data: ITournament) {
    this.tournamentId = data['tournamentId'];
    this.avatar = data['avatar'];
    this.tournamentName = data['tournamentName'];
    this.startDate = data['startDate'] ? new Date(data['startDate'] * 1000) : undefined;
    this.endDate = data['endDate'] ? new Date(data['endDate'] * 1000) : undefined;

    this.previousTournamentId = data['previousTournamentId'];
    this.nextTournamentId = data['nextTournamentId'];

    if (!!data['matches']) {
      this.matches = data['matches'].map((matchData: any) => new Match(matchData)).sort((a, b) => (a.startDate?.getTime() ?? 0) - (b.startDate?.getTime() ?? 0))
    }

    if (!!data['teams']) {
      this.teams = data['teams'].map((teamData: any) => new Team(teamData));
    }

    if (!!data['matchBets']) {
      this.matchBets = data['matchBets'].map((matchBetData: any) => new MatchBet(matchBetData));
    }
  }

  public toJson(): any {
    return {
      tournamentId: this.tournamentId,
      avatar: this.avatar,
      tournamentName: this.tournamentName,
      startDate: !!this.startDate ? this.startDate.getTime() / 1000 : undefined,
      endDate: this.endDate ? this.endDate.getTime() / 1000 : undefined,
    }
  }
}
