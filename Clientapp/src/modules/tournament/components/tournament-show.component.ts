import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../shared/services/api.service";
import {Tournament} from "../../shared/models/tournament";
import {Match} from "../../shared/models/match";

export type TournamentTab = 'stats' | 'ranking' | 'matches';

@Component({
  selector: 'tournament-show',
  templateUrl: './tournament-show.component.html',
  styleUrls: [
    'tournament-show.component.less'
  ]
})
export class TournamentShowComponent implements OnInit, OnDestroy {

  private _currentTab: TournamentTab = 'matches';
  public set currentTab(value: TournamentTab) {
    this._currentTab = value;
    this.router.navigate([], { fragment: value }).then(() => {});
  }
  public get currentTab(): TournamentTab { return this._currentTab; }

  private _tournament: Tournament = new Tournament({tournamentName: '' });

  get tournament(): Tournament {
    return this._tournament;
  }

  get tournamentName(): string {
    return this._tournament.tournamentName;
  }

  get tournamentMatches(): Match[] {
    return this._tournament.matches;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private api: ApiService
  ) {

  }

  ngOnInit() {
    this.activatedRoute.fragment.subscribe(fragment => {
      if (fragment === 'stats' || fragment === 'ranking' || fragment === 'matches')
        this._currentTab = fragment;
    });

    this.activatedRoute.paramMap.subscribe(async params => {
      const id = params.get('id');
      const parsedId = parseInt(id as string);
      if (isNaN(parsedId)) {
        await this.router.navigate(['error404']);
        return;
      }
      try {
        this._tournament = await this.api.readTournament(parsedId);
      }
      catch (e: any) {
        await this.router.navigate(['error404']);
      }

    });
  }

  ngOnDestroy() {
  }
}
