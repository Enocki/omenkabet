<?php

namespace Pickomenka\Exceptions;

class InternalErrorException extends \Exception
{
    private readonly mixed $reason;

    public function __construct(mixed $reason)
    {
        parent::__construct();
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReason(): mixed
    {
        return $this->reason;
    }
}