# Pickomenka

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Run an apache instance at the root directory of the project to be able to access the api.

## Build

Run the `build.bat` script to build the project. The build artifacts will be stored in the `build/` directory.
